package nabla2.webapi.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.webapi.model.Operation

import java.util.function.Function

trait ReportTrait<T> {

  abstract SingleLineText getName()
  abstract StrictJavaIdentifier getIdentifier()
  abstract Operation getOperation()
  abstract SingleLineText getLayoutFile()
  abstract SingleLineText getFileFormat()

  /**
   * この操作のエンドポイントURLを返す。
   * @param replacer URL中のパスパラメータの値を返す関数
   * @return エンドポイントURL
   */
  @Memoized
  String getEndPointUrl(Function<String, String> replacer) {
    "${operation.getEndPointUrl(replacer)}/${identifier}.${downloadFilesExtension}"
  }

  /**
   * このレポートの識別子を返す。
   * @return このレポートの識別子
   */
  String getKey() {
    identifier.lowerCamelized.get()
  }

  /**
   * この帳票のレイアウト定義ファイルを取得する。
   * @return レイアウト定義ファイル
   */
  @Memoized
  File getReportsLayoutFile() {
    File dir = new File(this._table.name.book.path).parentFile
    new File(dir, layoutFile.get())
  }

  /**
   * この帳票のレイアウト定義ファイルのパスを返す。
   * @return レイアウト定義ファイルのパス
   */
  @Memoized
  String getLayoutFilePath() {
    String extension = csvReporting ? 'json'
                     : forDownload  ? 'xlsx'
                     : 'xml'
    "${operation.namespace.replace('.', '/')}/${identifier.upperCamelized.get()}.${extension}"
  }

  String getTransformerName() {
    fileFormat.value.map {
      it.toLowerCase() == 'handsontable' ? 'Handsontable' : 'Poi'
    }.orElse('Poi')
  }

  String getDownloadFilesExtension() {
    fileFormat.value.map {
      it.toLowerCase() == 'handsontable' ? 'json' : it.toLowerCase()
    }.orElse('json')
  }

  @Memoized
  boolean isCsvReporting() {
    downloadFilesExtension == 'csv'
  }

  @Memoized
  boolean isExcelReporting() {
    fileFormat.value.map{ format ->
      format.toLowerCase().with{it == 'handsontable' || it == 'xlsx'}
    }
  }

  String getContentType() {
    downloadFilesExtension.with {
      (it == 'csv')  ? 'text/csv' :
      (it == 'json') ? 'application/json' :
      (it == 'xlsx') ? 'application/octet-stream' :
                       'text/plain'
    }
  }

  /**
   * ダウンロード帳票処理かどうかを返す。
   * @return ダウンロード帳票処理であればtrue
   */
  @Memoized
  boolean isForDownload() {
    operation
      .httpMehtod.value
      .map{it.toLowerCase() == 'get'}
      .orElse(false)
  }


  /**
   * アップロード帳票処理かどうかを返す。
   * @return アップロード帳票処理であればtrue
   */
  @Memoized
  boolean isForUpload() {
    !forDownload
  }
}