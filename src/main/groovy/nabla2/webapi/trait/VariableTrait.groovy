package nabla2.webapi.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.webapi.model.Operation
import nabla2.webapi.model.VariableType
import nabla2.webapi.template.variable.JavaVariableCodeSnippet

trait VariableTrait<T> {

  abstract VariableType getType()
  abstract ParameterizedText getTypeDefinition()
  abstract Operation getRestOperation()
  abstract StrictJavaIdentifier getIdentifier()


  @Memoized
  List<VariableTypeOption> getTypeOptions() {
    type.typeDefinition.captureWithName(typeDefinition.literal.get()).collect { each ->
      new VariableTypeOption(
        name: each.key,
        value: each.value,
        model: this,
      )
    }
  }

  @Memoized
  String getVarName() {
    identifier.lowerCamelized.get()
  }

  @Memoized
  JavaVariableCodeSnippet getSnippet() {
    type.codeSnippetFor(this)
  }
}
