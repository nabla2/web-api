package nabla2.webapi.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.webapi.model.Operation
import nabla2.webapi.model.ProcessStepDefinition
import nabla2.webapi.model.Variable

trait ProcessStepTrait<T> {

  abstract ProcessStepDefinition getDefinition()
  abstract ParameterizedText getContent()
  abstract MultiLineText getArgumentsNames()
  abstract Operation getRestOperation()
  abstract StrictJavaIdentifier getIdentifier()

  @Memoized
  List<ProcessStepParam> getParams() {
    definition.content.captureWithName(content.literal.get()).collect { each ->
      new ProcessStepParam(
        name: each.key,
        value: each.value,
        model: this,
      )
    }
  }

  @Memoized
  List<Variable> getArguments() {
    argumentsNames.value.map{ names ->
      names.tokenize('\n').collect{it.trim()}.collect { name ->
        Variable found = restOperation.processVariables.find{ it.name.sameAs(name) }
        if (!found) throw new IllegalArgumentException(
          "Unknown variable name: ${name}\n${this}"
        )
        found
      }
    }.orElse([])
  }

  @Memoized
  List<String> getArgumentSignatures() {
    arguments.collect{"${it.snippet.javaType} ${it.varName}"}
  }

  @Memoized
  List<String> getArgumentNames() {
    arguments.collect{it.varName}
  }

  @Memoized
  String getMethodDeclaration() {
    definition.codeSnippetFor(this).declarationCodeSnippet
  }

  @Memoized
  String getMethodInvocation() {
    definition.codeSnippetFor(this).invocationCodeSnippet
  }

  @Memoized
  String getMethodName() {
    identifier.lowerCamelized.get()
  }
}
