package nabla2.webapi.trait

import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.webapi.model.ProcessStep
import nabla2.webapi.model.Variable
import nabla2.webapi.template.step.JavaMethodCodeSnippet
import nabla2.webapi.template.variable.JavaVariableCodeSnippet

trait VariableTypeTrait<T> {

  abstract JavaNamespace getTemplateNamespace()
  abstract StrictJavaIdentifier getTemplateIdentifier()

  JavaVariableCodeSnippet codeSnippetFor(Variable var) {
    String fqn = "${templateNamespace}.${templateIdentifier.upperCamelized.get()}"
    (JavaVariableCodeSnippet) Class.forName(fqn).newInstance(var)
  }
}
