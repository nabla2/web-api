package nabla2.webapi.trait

import nabla2.metamodel.datatype.SingleLineText
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.Variable


trait DataItemMappingTrait<T> {

  abstract Variable getTargetVariable()
  abstract SingleLineText getTargetItemName()
  abstract DataItem getDataItem()

  String valueRetrievalCodeSnippet(String refName, int shift) {
    refName = refName ? refName : targetVariable.varName
    String itemName = targetItemName.value.orElse('')
    targetVariable.snippet.retrievalCodeSnippet(refName, itemName)
  }

  String valueAssignmentCodeSnippet(String refName, int shift) {
    String itemName = targetItemName.value.orElse('')
    String assignee = dataItem.requestMappingCodeSnippet(refName)
    targetVariable.snippet.assignmentCodeSnippet(targetVariable.varName, itemName, assignee)
  }
}
