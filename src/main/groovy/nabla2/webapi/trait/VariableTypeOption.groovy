package nabla2.webapi.trait

import groovy.transform.Canonical
import nabla2.persistence.model.DbTable
import nabla2.webapi.model.ProcessStep
import nabla2.webapi.model.Variable

@Canonical
class VariableTypeOption {
  Variable model
  String name
  String value

  DbTable getAsTable() {
    model.project.tables.find{ it.name.sameAs(value) }.with { table ->
      if (!table) throw new IllegalArgumentException(
        "Unknown table name: ${value}\n${model}"
      )
      table
    }
  }
}
