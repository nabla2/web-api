package nabla2.webapi.trait

import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.webapi.model.ProcessStep
import nabla2.webapi.template.step.JavaMethodCodeSnippet

trait ProcessStepDefinitionTrait<T> {

  abstract JavaNamespace getNamespace()
  abstract StrictJavaIdentifier getIdentifier()

  JavaMethodCodeSnippet codeSnippetFor(ProcessStep step) {
    String fqn = "${namespace}.${identifier.upperCamelized.get()}"
    (JavaMethodCodeSnippet) Class.forName(fqn).newInstance(step)
  }
}
