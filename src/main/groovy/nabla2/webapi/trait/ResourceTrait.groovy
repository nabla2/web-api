package nabla2.webapi.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.persistence.model.DbTable
import nabla2.webapi.model.Service

trait ResourceTrait<T> {

  abstract StrictJavaIdentifier getIdentifier()
  abstract JavaNamespace getNamespace()
  abstract Service getService()

  List<DbTable> getTables() {
    service.project.tables
  }

  @Memoized
  String getInterfaceName() {
    "${identifier.className.get()}Resource"
  }

  @Memoized
  String getPayloadsNamespace() {
    "${namespace}.${identifier.underscorized.get()}"
  }

  @Memoized
  String getImplementerClassName() {
    "${interfaceName}Impl"
  }
}
