package nabla2.webapi.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.DataItemMapping
import nabla2.webapi.model.Operation
import static nabla2.metamodel.datatype.StrictJavaIdentifier.*

trait DataItemTrait<T> {

  abstract SingleLineText getName()
  abstract SingleLineText getType()
  abstract StrictJavaIdentifier getIdentifier()
  abstract List<DataItem> getHierarchy()
  abstract DataItem getParent()
  abstract SingleLineText getLocation()
  abstract Operation getOperation()
  abstract DataItemMapping getMapping()
  abstract List<DataItem> getChildren()

  /**
   * この項目が配列型かどうかを返す。
   * @return この項目が配列型であればtrue
   */
  boolean isArray() {
    type.get() == 'array'
  }

  /**
   * この項目がオブジェクト(辞書配列)かどうかを返す。
   * @return この項目が辞書配列であればtrue
   */
  boolean isObject() {
    type.get() == 'object'
  }

  String getKey() {
    identifier.lowerCamelized.get()
  }

  boolean isRequestParameter() {
    location.sameAs('リクエストパラメータ')
  }

  boolean isRequestPath() {
    location.sameAs('リクエストパス')
  }

  boolean isRequestBody() {
    location.sameAs('リクエストボディ')
  }

  boolean isResponseBody() {
    location.sameAs('レスポンスボディ')
  }

  boolean isRequestPayload() {
    operation.httpMehtod.sameAs('GET') ? isRequestParameter()
                                       : isRequestBody()
  }

  boolean isRequestData() {
    requestBody || requestPath || requestParameter
  }

  @Memoized
  String getSetterName() {
    "set${identifier.upperCamelized.get()}"
  }

  @Memoized
  String getGetterName() {
    "get${identifier.upperCamelized.get()}"
  }

  String getPayloadClassName() {
    responseBody ? operation.responsePayloadClassName
                 : operation.requestPayloadClassName
  }

  String getJavaType() {
    String dataType = type.get().toLowerCase()

    switch (dataType) {
      case 'string': return 'String'
      case 'number': return 'Long'
      case 'boolean': return 'Boolean'
      case 'array': return "List<${children.first().javaType}>"
      case 'object':
        return hash ? "Map<String, ${hashValue.javaType}>"
             : "${payloadClassName}.${identifier.upperCamelized.get()}"

      default: throw new IllegalArgumentException(
        "Unknown data type: ${dataType}\n${this}"
      )
    }
  }

  boolean isHash()  {
    type.sameAs('object') && children.any{it.type.sameAs('[key]')}
  }

  boolean isPlainObject() {
    type.sameAs('object') && !hash
  }

  boolean isHashKey() {
    type.sameAs('[key]')
  }

  @Memoized
  DataItem getHashKey() {
    children.find{ it.isHashKey() }
  }

  @Memoized
  DataItem getHashValue() {
    children.find{ !it.isHashKey() }
  }


  /**
   * ペイロードのデータと内部変数の受け渡しを行うコードスニペットを出力する。
   * @param destRef ペイロード参照名
   * @return コードスニペット
   */
  String mappingJavaCodeSnippet(String payloadRefName) {

      requestData ? requestMappingCodeSnippet(payloadRefName)
                : responseMappingCodeSnippet(payloadRefName)
  }

  /**
   * リクエストペイロードからデータを受け取るJavaコードスニペットを出力する。
   * @param srcRef ペイロードもしくはそのプロパティの参照名
   * @param shift 出力コードのシフト幅
   * @param destRef 受け渡し先変数参照名
   * @return コードスニペット
   */
  String requestMappingCodeSnippet(String srcRef, int shift = 0, String varRef = null) {
    if (!mapping) throw new IllegalStateException(
      "This dataItem ${name} does not have any mappings\n${this}"
    )
    String PAD = ' ' * shift
    varRef = mapping.targetVariable.varName
    if (type.sameAs('string') || type.sameAs('[key]')) {
      return "${srcRef}.${getterName}()"
    }
    if (type.sameAs('number')) {
      return "${srcRef}.${getterName}().longValue()"
    }
    if (type.sameAs('boolean')) {
      return "${srcRef}.${getterName}()"
    }
    throw new IllegalArgumentException(
      "Unkown data type: ${type}"
    )
  }

  /**
   * ペイロードにデータを設定するJavaコードスニペットを出力する。
   * @param destRef ペイロードもしくはそのプロパティの参照名
   * @param shift 出力コードのシフト幅
   * @param srcRef 変数参照名
   * @return コードスニペット
   */
  String responseMappingCodeSnippet(String destRef, int shift = 0, String srcRef = null) {

    String PAD = ' ' * shift
    srcRef = srcRef ? srcRef : mapping.targetVariable.varName
    String lName = identifier.lowerCamelized.get()
    if (array) {
      if (children.empty) throw new IllegalStateException(
        "An array data type must have a child member\n${this}"
      )
      DataItem member = children.first()
      return """\
      |${PAD}${destRef}.${setterName}(${srcRef}.stream().map(each -> {
      |${member.responseMappingCodeSnippet(destRef, shift+4, 'each')}
      |${PAD}}).collect(Collectors.toList()));
      """.trim().stripMargin()
    }
    if (hash) {
      String put = "${lName}.put(key, "
      return """\
      |${PAD}${javaType} ${lName} = new HashMap<>();
      |${PAD}${srcRef}.forEach((key, value) -> {
      |${hashValue.responseMappingCodeSnippet(destRef, shift+4, 'value').replaceFirst(/^\s*[^(]+\(/, put)}
      |${PAD}});
      |${PAD}${destRef}.${setterName}(${lName});
      """.trim().stripMargin()
    }
    if (plainObject) {
      String objectRef = lowerCamelize(identifier.lowerCamelized.get())
      return """\
      |${PAD}${javaType} ${objectRef} = new ${javaType}();
      ${children.collect{ child -> """\
      |${child.responseMappingCodeSnippet(objectRef, shift+4, srcRef)}
      """.trim()}.join('\n')}
      ${parent && parent.array ? """\
      |${PAD}return ${objectRef};
      """:"""\
      |${PAD}${destRef}.${setterName}(${objectRef});
      """.trim()}
      """.trim().stripMargin()
    }
    if (!mapping) return ''
    if (type.sameAs('string')) {
      return """\
      |${PAD}${destRef}.${setterName}(${mapping.valueRetrievalCodeSnippet(srcRef, shift)});
      """.trim().stripMargin()
    }
    if (type.sameAs('number')) {
      return """\
      |${PAD}${destRef}.${setterName}(BigDecimal.valueOf(${mapping.valueRetrievalCodeSnippet(srcRef, shift)}));
      """.trim().stripMargin()
    }
    if (type.sameAs('boolean')) {
      return """\
      |${PAD}${destRef}.${setterName}(${mapping.valueRetrievalCodeSnippet(srcRef, shift)});
      """.trim().stripMargin()
    }
    throw new IllegalArgumentException(
      "Unkown data type: ${type}\n${this}"
    )
  }
}