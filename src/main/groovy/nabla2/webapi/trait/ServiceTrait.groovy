package nabla2.webapi.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.datatype.Url
import nabla2.metamodel.datatype.UrlWithPlaceholder

trait ServiceTrait<T> {


  abstract StrictJavaIdentifier getIdentifier()
  abstract Url getHost()
  abstract UrlWithPlaceholder getBasePath()

  /**
   * このサービスのエンドポイントURLを返す。
   * @return エンドポイントURL
   */
  @Memoized
  String getEndPoint() {
    "${host}/${basePath.literal.get()}"
    .replaceAll(/(?<!:)\/+/, '/')
    .replaceAll(/\/$/, '')
  }

  /**
   * このサービスに関連する定数のプレフィックス文字列を返す。
   * @return 定数のプレフィックス文字列
   */
  String getPrefixSymbol() {
    identifier.underscorized.get().toUpperCase()
  }
}
