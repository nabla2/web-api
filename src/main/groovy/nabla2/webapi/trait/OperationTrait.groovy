package nabla2.webapi.trait

import groovy.transform.Memoized
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.datatype.UrlWithPlaceholder
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.DataItemMapping
import nabla2.webapi.model.Project
import nabla2.webapi.model.Resource
import nabla2.webapi.model.Service

import java.util.function.Function

trait OperationTrait<T> {

  abstract Resource getResource()
  abstract UrlWithPlaceholder getPath()
  abstract List<DataItem> getDataItems()
  abstract SingleLineText getHttpMehtod()
  abstract StrictJavaIdentifier getIdentifier()

  @Memoized
  Project getProject() {
    resource.service.project
  }

  /**
   * この操作のエンドポイントURLを返す。
   * @param replacer URL中のパスパラメータの値を返す関数
   * @return エンドポイントURL
   */
  @Memoized
  String getEndPointUrl(Function<String, String> replacer) {
    Service service = resource.service
    [ '${ENV.' + service.prefixSymbol + '_ENDPOINT}',
      resource.basePath.embedParams(replacer).get(),
      path.embedParams(replacer).get(),
    ].join('/').replaceAll(/(?<!:)\/+/, '/')
  }

  @Memoized
  String getNamespace() {
    String id = resource.identifier.lowerCamelized.get()
               .replaceAll(/[A-Z]/) {String head -> '_' + head.toLowerCase()}
    "${resource.namespace}.${id}"
  }

  @Memoized
  String getLogicClassName() {
    identifier.upperCamelized.get() + 'Logic'
  }

  @Memoized
  List<DataItem> getPathParameters() {
    dataItems.findAll{ it.requestPath }
  }

  @Memoized
  List<DataItem> getRequestBody() {
    dataItems.findAll{ it.requestBody }
  }

  @Memoized
  List<DataItem> getRequestParameters() {
    dataItems.findAll{ it.requestParameter }
  }

  @Memoized
  List<DataItem> getResponseBody() {
    dataItems.findAll{ it.responseBody }
  }

  @Memoized
  List<DataItem> getResponseBodyTopLevelItems() {
    responseBody.findAll{ it.topLevel }
  }

  @Memoized
  List<DataItem> getRequestPayload() {
    httpMehtod.sameAs('GET') ? requestParameters
                             : requestBody
  }

  @Memoized
  List<DataItem> getMappedRequestItems() {
    requestData.findAll{ it.mapping }
  }

  @Memoized
  String getRequestPayloadClassName() {
    "${identifier.upperCamelized.get()}Request"
  }

  @Memoized
  String getResponsePayloadClassName() {
    "${identifier.upperCamelized.get()}Response"
  }

  @Memoized
  String getMethodName() {
    identifier.lowerCamelized.get()
  }

  @Memoized
  List<DataItem> getRequestData() {
    dataItems.findAll {
      it.requestBody || it.requestParameter || it.requestPath
    }
  }

}
