package nabla2.webapi.trait

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.persistence.model.DbTable
import nabla2.webapi.model.ProcessStep
import nabla2.webapi.model.Variable

@Canonical
class ProcessStepParam {
  ProcessStep model
  String name
  String value

  Variable getAsVariable() {
    model.restOperation.processVariables.find{ it.name.sameAs(value) }.with { var ->
      if (!var) throw new IllegalArgumentException(
        "Unknown variable name: ${value}\n${model}"
      )
      var
    }
  }

  @Memoized
  DbTable getAsTable() {
    model.project.tables.find{ it.name.sameAs(value) }.with { table ->
      if (!table) throw new IllegalArgumentException(
        "Unknown table name: ${value}\n${model}"
      )
      table
    }
  }
}
