package nabla2.webapi.gradle

import nabla2.metamodel.model.GeneratorOption
import groovy.transform.Canonical
import groovy.transform.Memoized

/**
 * RestAPIサービスを生成オプション定義
 *
 * @author nabla2.metamodel.generator
 */
@Canonical
class GenerateRestApiServiceOptionSet {

  List<GeneratorOption> allOptions
  Map<String, ?> properties

  /**
   * 環境フラグを表すビルドプロパティを取得する。
   */
  String getEnv() {
    properties.find{it.key.toLowerCase() == 'env'}.with {
      (it == null || it.value.toString().trim().empty) ? '' : it.value.toString().toUpperCase()
    }
  }

  /**
   * RestAPI生成用変数
   */
  @Memoized
  Map<String, String> getRestServiceGeneratorVariables() {
    Map<String, String> result = [:]
    def defaults = allOptions.findAll{ !it.value.empty && it.environment.value.map{it.toUpperCase() == 'ANY'}.orElse(false) }
    def forCurrentEnv = allOptions.findAll{ !it.value.empty && it.environment.value.map{it.toUpperCase() == env}.orElse(false) }
    defaults.forEach{ result[it.name.get()] = it.value.value.orElse('') }
    forCurrentEnv.forEach{ result[it.name.get()] = it.value.value.orElse('') }
    result
  }
}