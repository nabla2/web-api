package nabla2.webapi.gradle

import io.reactivex.Observable
import nabla2.excel.ExcelWorkbookLoader
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.GeneratorOption
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.Table
import nabla2.webapi.model.DataItemMappingValidator
import nabla2.webapi.model.DataItemValidator
import nabla2.webapi.model.FieldValidator
import nabla2.webapi.model.FieldValidatorValidator
import nabla2.webapi.model.FormatValidator
import nabla2.webapi.model.Operation
import nabla2.webapi.model.OperationValidator
import nabla2.webapi.model.ProcessStepDefinitionValidator
import nabla2.webapi.model.ProcessStepValidator
import nabla2.webapi.model.Project
import nabla2.webapi.model.ProjectValidator
import nabla2.webapi.model.ReportValidator
import nabla2.webapi.model.Resource
import nabla2.webapi.model.ResourceValidator
import nabla2.webapi.model.ServiceValidator
import nabla2.webapi.model.VariableTypeValidator
import nabla2.webapi.model.VariableValidator
import nabla2.webapi.template.BuildGradle
import nabla2.webapi.template.CommonUtilsJava
import nabla2.webapi.template.ErrorHandlerJava
import nabla2.webapi.template.FieldValidatorJava
import nabla2.webapi.template.InvalidInputExceptionJava
import nabla2.webapi.template.RestRequestPayloadJava
import nabla2.webapi.template.RestResourceControllerJava
import nabla2.webapi.template.RestResourceDummyImplJava
import nabla2.webapi.template.RestResourceInterfaceJava
import nabla2.webapi.template.RestResponsePayloadJava
import nabla2.webapi.template.SpringBootApplicationJava
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
/**
 * RestAPIサービスを生成(ダミー実装)
 *
 * @author nabla2.metamodel.generator
 */
class GenerateRestApiServiceWithDummyImpl extends DefaultTask {

  @InputFiles FileCollection workbooks
  @InputFiles @Optional FileCollection referringTo
  @OutputDirectory @Optional File springBootApplicationJavaDir
  @OutputDirectory @Optional File restResourceControllerJavaDir
  @OutputDirectory @Optional File restResourceInterfaceJavaDir
  @OutputDirectory @Optional File restRequestPayloadJavaDir
  @OutputDirectory @Optional File restResponsePayloadJavaDir
  @OutputDirectory @Optional File invalidInputExceptionJavaDir
  @OutputDirectory @Optional File commonUtilsJavaDir
  @OutputDirectory @Optional File errorHandlerJavaDir
  @OutputDirectory @Optional File fieldValidatorJavaDir
  @OutputDirectory @Optional File restResourceDummyImplJavaDir
  File baseDir
  @OutputFile @Optional File errorReport
  String group = 'laplacian generator'
  String description = 'SpringBootベースのRestAPIサービスを生成する(Restリソースの実装はダミー)'

  @TaskAction
  void exec() {
    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
      ExcelWorkbookLoader.from(workbook).table$
    }.with { _table$ ->
      errorReport.text = ''
      Throwable error = null
      Map<String, Map<String, String>> commonProps = ModelCommonProperty.from(_table$).toList().blockingGet().inject([:]) { acc, prop ->
        Map<String,String> bucket = acc[prop.entityName.get()]
        if (prop.value.empty) return acc
        if (!bucket) {
          bucket = [:]
          acc[prop.entityName.get()] = bucket
        }
        bucket[prop.propertyName.get()] = prop.value.get()
        acc
      }
      def table$ = _table$.map{ table ->
        table.copyWith(globalMetadata: commonProps)
      }
      validate(table$)
      List<GeneratorOption> optionList = GeneratorOption.from(table$).filter{
        it.optionType.targetTask.identifier.sameAs('GenerateRestApiServiceWithDummyImpl')
      }.toList().blockingGet()
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new BuildGradle(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new SpringBootApplicationJava(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Resource.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new RestResourceControllerJava(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Resource.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new RestResourceInterfaceJava(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Operation.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new RestRequestPayloadJava(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Operation.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new RestResponsePayloadJava(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new InvalidInputExceptionJava(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new CommonUtilsJava(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new ErrorHandlerJava(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      FieldValidator.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new FieldValidatorJava(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Resource.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        new RestResourceDummyImplJava(it).with { template ->
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
    }
  }

  void validate(Observable<Table> table$) {
    Throwable error
    println 'Checking model files...'
    Observable.fromArray(
      new ProjectValidator().validate(table$),
      new ServiceValidator().validate(table$),
      new ResourceValidator().validate(table$),
      new OperationValidator().validate(table$),
      new ReportValidator().validate(table$),
      new DataItemValidator().validate(table$),
      new FormatValidator().validate(table$),
      new FieldValidatorValidator().validate(table$),
      new DataItemMappingValidator().validate(table$),
      new ProcessStepValidator().validate(table$),
      new VariableValidator().validate(table$),
      new VariableTypeValidator().validate(table$),
      new ProcessStepDefinitionValidator().validate(table$),
    )
    .flatMap {it}
    .doOnNext{ println it; errorReport << it }
    .doOnError{ it.printStackTrace(); error = it }
    .onErrorResumeNext(Observable.empty())
    .toList()
    .blockingGet().with {
      if (!it.empty) throw new GradleException(
        "There are ${it.size()} errors in model files. see details in ${this.errorReport.path}"
      )
      if (error) throw new GradleException(
        "An error occurred while validating the model files.", error
      )
    }
  }

  static class Settings extends TaskSettings<GenerateRestApiServiceWithDummyImpl> {
    FileCollection workbooks
    FileCollection referringTo
    File baseDir
    File errorReport
    void setWorkbook(File workbook) {
      workbooks = project.files(workbook)
    }
    @Override
    void setup(GenerateRestApiServiceWithDummyImpl task) {
      File baseDir = this.baseDir ?: new File('./')
      task.workbooks = workbooks
      task.referringTo = referringTo ?: project.files([])
      task.baseDir = baseDir
      task.errorReport = errorReport ?: new File('./build/laplacian/errors.md')
      task.springBootApplicationJavaDir = new File(baseDir, 'src/main/java')
      task.restResourceControllerJavaDir = new File(baseDir, 'src/main/java')
      task.restResourceInterfaceJavaDir = new File(baseDir, 'src/main/java')
      task.restRequestPayloadJavaDir = new File(baseDir, 'src/main/java')
      task.restResponsePayloadJavaDir = new File(baseDir, 'src/main/java')
      task.invalidInputExceptionJavaDir = new File(baseDir, 'src/main/java')
      task.commonUtilsJavaDir = new File(baseDir, 'src/main/java')
      task.errorHandlerJavaDir = new File(baseDir, 'src/main/java')
      task.fieldValidatorJavaDir = new File(baseDir, 'src/main/java')
      task.restResourceDummyImplJavaDir = new File(baseDir, 'src/main/java')
    }
  }
}