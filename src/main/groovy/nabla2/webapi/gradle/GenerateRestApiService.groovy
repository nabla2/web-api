package nabla2.webapi.gradle

import io.reactivex.Observable
import nabla2.excel.ExcelWorkbookLoader
import nabla2.gradle.TaskSettings
import nabla2.metamodel.model.GeneratorOption
import nabla2.metamodel.model.LocalizedTextBundle
import nabla2.metamodel.model.ModelCommonProperty
import nabla2.metamodel.model.Table
import nabla2.metamodel.template.ResourceBundleProperties
import nabla2.persistence.model.DbAccessMethod
import nabla2.persistence.model.DbTable
import nabla2.persistence.template.JpaEntityClassJava
import nabla2.persistence.template.JpaRepositoryJava
import nabla2.persistence.template.MirageSqlCustomQuerySql
import nabla2.persistence.template.MirageSqlRepositoryBaseJava
import nabla2.persistence.template.MirageSqlRepositoryCustomJava
import nabla2.persistence.template.RepositoryCustomJava
import nabla2.webapi.model.DataItemMappingValidator
import nabla2.webapi.model.DataItemValidator
import nabla2.webapi.model.FieldValidator
import nabla2.webapi.model.FieldValidatorValidator
import nabla2.webapi.model.FormatValidator
import nabla2.webapi.model.Operation
import nabla2.webapi.model.OperationValidator
import nabla2.webapi.model.ProcessStepDefinitionValidator
import nabla2.webapi.model.ProcessStepValidator
import nabla2.webapi.model.Project
import nabla2.webapi.model.ProjectValidator
import nabla2.webapi.model.Report
import nabla2.webapi.model.ReportValidator
import nabla2.webapi.model.Resource
import nabla2.webapi.model.ResourceValidator
import nabla2.webapi.model.ServiceValidator
import nabla2.webapi.model.VariableTypeValidator
import nabla2.webapi.model.VariableValidator
import nabla2.webapi.template.ApplicationProperties
import nabla2.webapi.template.BuildGradle
import nabla2.webapi.template.CommonUtilsJava
import nabla2.webapi.template.CsvReportingUtilJava
import nabla2.webapi.template.ErrorHandlerJava
import nabla2.webapi.template.FieldValidatorJava
import nabla2.webapi.template.GradleProperties
import nabla2.webapi.template.InvalidInputExceptionJava
import nabla2.webapi.template.MirageSqlSettingJava
import nabla2.webapi.template.ReportLayoutFile
import nabla2.webapi.template.RestOperationLogicJava
import nabla2.webapi.template.RestRequestPayloadJava
import nabla2.webapi.template.RestResourceControllerJava
import nabla2.webapi.template.RestResourceImplJava
import nabla2.webapi.template.RestResourceInterfaceJava
import nabla2.webapi.template.RestResponsePayloadJava
import nabla2.webapi.template.SpreadSheetReportingUtilJava
import nabla2.webapi.template.SpringBootApplicationJava
import nabla2.webapi.template.SystemMessageJava
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
/**
 * RestAPIサービスを生成
 *
 * @author nabla2.metamodel.generator
 */
class GenerateRestApiService extends DefaultTask {

  @InputFiles FileCollection workbooks
  @InputFiles @Optional FileCollection referringTo
  @OutputDirectory @Optional File applicationPropertiesDir
  @OutputDirectory @Optional File resourceBundlePropertiesDir
  @OutputDirectory @Optional File systemMessageJavaDir
  @OutputDirectory @Optional File springBootApplicationJavaDir
  @OutputDirectory @Optional File restResourceControllerJavaDir
  @OutputDirectory @Optional File restResourceInterfaceJavaDir
  @OutputDirectory @Optional File restRequestPayloadJavaDir
  @OutputDirectory @Optional File restResponsePayloadJavaDir
  @OutputDirectory @Optional File invalidInputExceptionJavaDir
  @OutputDirectory @Optional File commonUtilsJavaDir
  @OutputDirectory @Optional File errorHandlerJavaDir
  @OutputDirectory @Optional File fieldValidatorJavaDir
  @OutputDirectory @Optional File restResourceImplJavaDir
  @OutputDirectory @Optional File restOperationLogicJavaDir
  @OutputDirectory @Optional File mirageSqlSettingJavaDir
  @OutputDirectory @Optional File jpaEntityClassJavaDir
  @OutputDirectory @Optional File jpaRepositoryJavaDir
  @OutputDirectory @Optional File repositoryCustomJavaDir
  @OutputDirectory @Optional File mirageSqlRepositoryCustomJavaDir
  @OutputDirectory @Optional File mirageSqlRepositoryBaseJavaDir
  @OutputDirectory @Optional File mirageSqlCustomQuerySqlDir
  @OutputDirectory @Optional File reportLayoutFileDir
  @OutputDirectory @Optional File spreadSheetReportingUtilJavaDir
  @OutputDirectory @Optional File csvReportingUtilJavaDir
  File baseDir
  @OutputFile @Optional File errorReport
  String group = 'laplacian generator'
  String description = 'Restリソースインタフェースの実装系を生成する'

  @TaskAction
  void exec() {
    Observable.fromIterable(workbooks + referringTo).flatMap { workbook ->
      ExcelWorkbookLoader.from(workbook).table$
    }.with { _table$ ->
      errorReport.text = ''
      Throwable error = null
      Map<String, Map<String, String>> commonProps = ModelCommonProperty.from(_table$).toList().blockingGet().inject([:]) { acc, prop ->
        Map<String,String> bucket = acc[prop.entityName.get()]
        if (prop.value.empty) return acc
        if (!bucket) {
          bucket = [:]
          acc[prop.entityName.get()] = bucket
        }
        bucket[prop.propertyName.get()] = prop.value.get()
        acc
      }
      def table$ = _table$.map{ table ->
        table.copyWith(globalMetadata: commonProps)
      }
      validate(table$)
      List<GeneratorOption> optionList = GeneratorOption.from(table$).filter{
        it.optionType.targetTask.identifier.sameAs('GenerateRestApiService')
      }.toList().blockingGet()
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new BuildGradle(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new GradleProperties(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, './'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new ApplicationProperties(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/resources'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      LocalizedTextBundle.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new ResourceBundleProperties(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/resources'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new SystemMessageJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new SpringBootApplicationJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Resource.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new RestResourceControllerJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Resource.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new RestResourceInterfaceJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Operation.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new RestRequestPayloadJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Operation.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new RestResponsePayloadJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new InvalidInputExceptionJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new CommonUtilsJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new ErrorHandlerJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      FieldValidator.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new FieldValidatorJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Resource.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new RestResourceImplJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Operation.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new RestOperationLogicJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new MirageSqlSettingJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbTable.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new JpaEntityClassJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbTable.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new JpaRepositoryJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbTable.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new RepositoryCustomJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbTable.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new MirageSqlRepositoryCustomJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbTable.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new MirageSqlRepositoryBaseJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      DbAccessMethod.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new MirageSqlCustomQuerySql(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/resources'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Report.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new ReportLayoutFile(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/resources'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new SpreadSheetReportingUtilJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
      Project.from(table$)
      .filter {
        referringTo.every{book -> book.path != it._row.cells.first().book.path}
      }
      .doOnNext {
        def opts = new GenerateRestApiServiceOptionSet(optionList, project.properties)
        new CsvReportingUtilJava(it).with { template ->
          template.metaClass.properties.find{it.type == GenerateRestApiServiceOptionSet}.with { p ->
            if (p) p.setProperty(template, opts)
          }
          template.regenerationMark = "@author nabla2.metamodel.generator"
          template.generateTo(new File(baseDir, 'src/main/java'))
        }
      }
      .doOnError{it.printStackTrace(); error = it}
      .onErrorResumeNext(Observable.empty())
      .toList()
      .blockingGet().with {
        if (error) throw new GradleException(
          "An error occurred while processing the model files."
        )
      }
    }
  }

  void validate(Observable<Table> table$) {
    Throwable error
    println 'Checking model files...'
    Observable.fromArray(
      new ProjectValidator().validate(table$),
      new ServiceValidator().validate(table$),
      new ResourceValidator().validate(table$),
      new OperationValidator().validate(table$),
      new ReportValidator().validate(table$),
      new DataItemValidator().validate(table$),
      new FormatValidator().validate(table$),
      new FieldValidatorValidator().validate(table$),
      new DataItemMappingValidator().validate(table$),
      new ProcessStepValidator().validate(table$),
      new VariableValidator().validate(table$),
      new VariableTypeValidator().validate(table$),
      new ProcessStepDefinitionValidator().validate(table$),
    )
    .flatMap {it}
    .doOnNext{ println it; errorReport << it }
    .doOnError{ it.printStackTrace(); error = it }
    .onErrorResumeNext(Observable.empty())
    .toList()
    .blockingGet().with {
      if (!it.empty) throw new GradleException(
        "There are ${it.size()} errors in model files. see details in ${this.errorReport.path}"
      )
      if (error) throw new GradleException(
        "An error occurred while validating the model files.", error
      )
    }
  }

  static class Settings extends TaskSettings<GenerateRestApiService> {
    FileCollection workbooks
    FileCollection referringTo
    File baseDir
    File errorReport
    void setWorkbook(File workbook) {
      workbooks = project.files(workbook)
    }
    @Override
    void setup(GenerateRestApiService task) {
      File baseDir = this.baseDir ?: new File('./')
      task.workbooks = workbooks
      task.referringTo = referringTo ?: project.files([])
      task.baseDir = baseDir
      task.errorReport = errorReport ?: new File('./build/laplacian/errors.md')
      task.applicationPropertiesDir = new File(baseDir, 'src/main/resources')
      task.resourceBundlePropertiesDir = new File(baseDir, 'src/main/resources')
      task.systemMessageJavaDir = new File(baseDir, 'src/main/java')
      task.springBootApplicationJavaDir = new File(baseDir, 'src/main/java')
      task.restResourceControllerJavaDir = new File(baseDir, 'src/main/java')
      task.restResourceInterfaceJavaDir = new File(baseDir, 'src/main/java')
      task.restRequestPayloadJavaDir = new File(baseDir, 'src/main/java')
      task.restResponsePayloadJavaDir = new File(baseDir, 'src/main/java')
      task.invalidInputExceptionJavaDir = new File(baseDir, 'src/main/java')
      task.commonUtilsJavaDir = new File(baseDir, 'src/main/java')
      task.errorHandlerJavaDir = new File(baseDir, 'src/main/java')
      task.fieldValidatorJavaDir = new File(baseDir, 'src/main/java')
      task.restResourceImplJavaDir = new File(baseDir, 'src/main/java')
      task.restOperationLogicJavaDir = new File(baseDir, 'src/main/java')
      task.mirageSqlSettingJavaDir = new File(baseDir, 'src/main/java')
      task.jpaEntityClassJavaDir = new File(baseDir, 'src/main/java')
      task.jpaRepositoryJavaDir = new File(baseDir, 'src/main/java')
      task.repositoryCustomJavaDir = new File(baseDir, 'src/main/java')
      task.mirageSqlRepositoryCustomJavaDir = new File(baseDir, 'src/main/java')
      task.mirageSqlRepositoryBaseJavaDir = new File(baseDir, 'src/main/java')
      task.mirageSqlCustomQuerySqlDir = new File(baseDir, 'src/main/resources')
      task.reportLayoutFileDir = new File(baseDir, 'src/main/resources')
      task.spreadSheetReportingUtilJavaDir = new File(baseDir, 'src/main/java')
      task.csvReportingUtilJavaDir = new File(baseDir, 'src/main/java')
    }
  }
}