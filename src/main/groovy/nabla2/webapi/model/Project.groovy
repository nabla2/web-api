package nabla2.webapi.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.datatype.UrlWithPlaceholder
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.webapi.model.Service
import nabla2.webapi.model.FieldValidator
import nabla2.webapi.model.ProcessStepDefinition
import nabla2.metamodel.model.LocalizedTextBundle
import nabla2.persistence.model.DbTable
import nabla2.persistence.model.DbDataset
/**
 * プロジェクト
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Project {

  /** 論理名 */
  static final String ENTITY_NAME = "プロジェクト"

  // ----- プロパティ定義 ------ //

  /**
   * 名称
   */
  SingleLineText name
  /**
   * 名前空間
   */
  JavaNamespace namespace
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * システムメッセージ定義
   */
  SingleLineText systemMessageDefinition
  /**
   * ベースパス
   */
  UrlWithPlaceholder basePath
  /**
   * 入力可能文字
   */
  SingleLineText charactersAllowedToInput
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * サービスリスト
   */
  @Memoized
  @JsonIgnore
  List<Service> getServices() {
    Service.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * 単項目精査バリデータ
   */
  @Memoized
  @JsonIgnore
  List<FieldValidator> getFieldValidators() {
    FieldValidator.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * 処理ステップ定義リスト
   */
  @Memoized
  @JsonIgnore
  List<ProcessStepDefinition> getProcessSteps() {
    ProcessStepDefinition.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * システムメッセージ
   */
  @Memoized
  @JsonIgnore
  List<LocalizedTextBundle> getSystemMessage() {
    LocalizedTextBundle.from(_table$).filter {
      this.systemMessageDefinition.sameAs(it.name)
    }.toList().blockingGet()
  }
  /**
   * テーブルリスト
   */
  @Memoized
  @JsonIgnore
  List<DbTable> getTables() {
    DbTable.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * データセットリスト
   */
  @Memoized
  @JsonIgnore
  List<DbDataset> getDatasets() {
    DbDataset.from(_table$).filter {
      this.name.sameAs(it.projectName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Project> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Project(
            name : new SingleLineText(row['名称']),
            namespace : new JavaNamespace(row['名前空間']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            systemMessageDefinition : new SingleLineText(row['システムメッセージ定義'] ?: "\u30B7\u30B9\u30C6\u30E0\u30E1\u30C3\u30BB\u30FC\u30B8"),
            basePath : new UrlWithPlaceholder(row['ベースパス']),
            charactersAllowedToInput : new SingleLineText(row['入力可能文字'] ?: "[^\\u0000-\\u001f]*"),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  /**
   * 完全修飾名
   */
  @Memoized
  String getFqn() {
    namespace + (namespace.empty ? '' : '.') + className
  }

  /**
   * クラス名
   */
  @Memoized
  String getClassName() {
    identifier.upperCamelized.orElse('')
  }

  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}