package nabla2.webapi.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.BooleanFlag
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.OrdinalNumber
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.metamodel.trait.Hierarchical
import nabla2.webapi.trait.DataItemTrait
import nabla2.webapi.model.Operation
import nabla2.webapi.model.Format
import nabla2.webapi.model.DataItemMapping
/**
 * データ項目
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class DataItem implements DataItemTrait<DataItem>, Hierarchical<DataItem> {

  /** 論理名 */
  static final String ENTITY_NAME = "データ項目"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * サービス名
   */
  SingleLineText serviceName
  /**
   * APIバージョン
   */
  SingleLineText apiVersion
  /**
   * 操作名
   */
  SingleLineText operationName
  /**
   * 区分
   */
  SingleLineText location
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * 親項目名
   */
  SingleLineText parentName
  /**
   * データ型
   */
  SingleLineText type
  /**
   * 必須
   */
  BooleanFlag required
  /**
   * 最大長
   */
  OrdinalNumber maxSize
  /**
   * フォーマット
   */
  SingleLineText formatName
  /**
   * 例
   */
  MultiLineText example
  /**
   * 詳細
   */
  MultiLineText description

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 操作
   */
  @Memoized
  @JsonIgnore
  Operation getOperation() {
    Operation.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.operationName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 親項目
   */
  @Memoized
  @JsonIgnore
  DataItem getParent() {
    DataItem.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.operationName.sameAs(it.operationName) &&
      this.location.sameAs(it.location) &&
      this.parentName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 子項目
   */
  @Memoized
  @JsonIgnore
  List<DataItem> getChildren() {
    DataItem.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.operationName.sameAs(it.operationName) &&
      this.location.sameAs(it.location) &&
      this.name.sameAs(it.parentName)
    }.toList().blockingGet()
  }
  /**
   * フォーマット
   */
  @Memoized
  @JsonIgnore
  Format getFormat() {
    Format.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.formatName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * データ項目マッピング
   */
  @Memoized
  @JsonIgnore
  DataItemMapping getMapping() {
    DataItemMapping.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.operationName.sameAs(it.operationName) &&
      this.location.sameAs(it.location) &&
      this.parentName.sameAs(it.parentDataItemName) &&
      this.name.sameAs(it.dataItemName)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<DataItem> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new DataItem(
            projectName : new SingleLineText(row['プロジェクト名']),
            serviceName : new SingleLineText(row['サービス名']),
            apiVersion : new SingleLineText(row['APIバージョン']),
            operationName : new SingleLineText(row['操作名']),
            location : new SingleLineText(row['区分']),
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            parentName : new SingleLineText(row['親項目名'] ?: "NO_PARENTS"),
            type : new SingleLineText(row['データ型']),
            required : new BooleanFlag(row['必須']),
            maxSize : new OrdinalNumber(row['最大長']),
            formatName : new SingleLineText(row['フォーマット'] ?: "NO_FORMAT"),
            example : new MultiLineText(row['例']),
            description : new MultiLineText(row['詳細']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}