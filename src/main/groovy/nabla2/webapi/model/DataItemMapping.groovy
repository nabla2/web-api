package nabla2.webapi.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.webapi.trait.DataItemMappingTrait
import nabla2.webapi.model.Operation
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.Variable
/**
 * データ項目マッピング
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class DataItemMapping implements DataItemMappingTrait<DataItemMapping> {

  /** 論理名 */
  static final String ENTITY_NAME = "データ項目マッピング"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * サービス名
   */
  SingleLineText serviceName
  /**
   * APIバージョン
   */
  SingleLineText apiVersion
  /**
   * 操作名
   */
  SingleLineText operationName
  /**
   * 区分
   */
  SingleLineText location
  /**
   * データ親項目名
   */
  SingleLineText parentDataItemName
  /**
   * データ項目名
   */
  SingleLineText dataItemName
  /**
   * 対象変数名
   */
  SingleLineText targetVariableName
  /**
   * 対象項目名
   */
  SingleLineText targetItemName

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * 操作
   */
  @Memoized
  @JsonIgnore
  Operation getOperation() {
    Operation.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.operationName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * データ項目
   */
  @Memoized
  @JsonIgnore
  DataItem getDataItem() {
    DataItem.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.operationName.sameAs(it.operationName) &&
      this.location.sameAs(it.location) &&
      this.parentDataItemName.sameAs(it.parentName) &&
      this.dataItemName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 対象変数
   */
  @Memoized
  @JsonIgnore
  Variable getTargetVariable() {
    Variable.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.operationName.sameAs(it.operationName) &&
      this.targetVariableName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<DataItemMapping> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new DataItemMapping(
            projectName : new SingleLineText(row['プロジェクト名']),
            serviceName : new SingleLineText(row['サービス名']),
            apiVersion : new SingleLineText(row['APIバージョン']),
            operationName : new SingleLineText(row['操作名']),
            location : new SingleLineText(row['区分']),
            parentDataItemName : new SingleLineText(row['データ親項目名'] ?: "NO_PARENTS"),
            dataItemName : new SingleLineText(row['データ項目名']),
            targetVariableName : new SingleLineText(row['対象変数名']),
            targetItemName : new SingleLineText(row['対象項目名']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}