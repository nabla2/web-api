package nabla2.webapi.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 操作バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class OperationValidator implements Validator<Operation> {
  Observable<ConstraintViolation<Operation>> validate(Observable<Table> table$) {
    Observable<Operation> entity$ = Operation.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Operation>> validateUniqueConstraint(Observable<Operation> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        serviceName : entity.serviceName.value.map{it.toString()}.orElse(null),
        apiVersion : entity.apiVersion.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      Operation another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<Operation> duplicated = it.get('__duplicated__')
      new ConstraintViolation<Operation>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, サービス名, APIバージョン, 名称',
          '操作',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<Operation>> validateCardinalityConstraint(Observable<Operation> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.resource == null) {
        violations.add(new ConstraintViolation<Operation>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'リソース',
            "プロジェクト名=${entity.projectName}, サービス名=${entity.serviceName}, APIバージョン=${entity.apiVersion}, 名称=${entity.resourceName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Operation>> validatePropertyType(Observable<Operation> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.serviceName.messageIfInvalid.map{['サービス名', it]}.orElse(null),
        entity.apiVersion.messageIfInvalid.map{['APIバージョン', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.resourceName.messageIfInvalid.map{['リソース名', it]}.orElse(null),
        entity.path.messageIfInvalid.map{['パス', it]}.orElse(null),
        entity.httpMehtod.messageIfInvalid.map{['HTTPメソッド', it]}.orElse(null),
        entity.description.messageIfInvalid.map{['詳細', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Operation>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Operation>> validateRequiredProperty(Observable<Operation> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.serviceName.literal.map{''}.orElse('サービス名'),
        entity.apiVersion.literal.map{''}.orElse('APIバージョン'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.resourceName.literal.map{''}.orElse('リソース名'),
        entity.path.literal.map{''}.orElse('パス'),
        entity.httpMehtod.literal.map{''}.orElse('HTTPメソッド'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Operation>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}