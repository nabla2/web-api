package nabla2.webapi.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.datatype.UrlWithPlaceholder
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.webapi.trait.ResourceTrait
import nabla2.webapi.model.Service
import nabla2.webapi.model.Operation
/**
 * リソース
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Resource implements ResourceTrait<Resource> {

  /** 論理名 */
  static final String ENTITY_NAME = "リソース"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * サービス名
   */
  SingleLineText sercieName
  /**
   * APIバージョン
   */
  SingleLineText apiVersion
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 名前空間
   */
  JavaNamespace namespace
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * ベースパス
   */
  UrlWithPlaceholder basePath
  /**
   * 詳細
   */
  MultiLineText description

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * サービス
   */
  @Memoized
  @JsonIgnore
  Service getService() {
    Service.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.sercieName.sameAs(it.name) &&
      this.apiVersion.sameAs(it.apiVersion)
    }.blockingFirst(null)
  }
  /**
   * 操作リスト
   */
  @Memoized
  @JsonIgnore
  List<Operation> getOperations() {
    Operation.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.sercieName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.name.sameAs(it.resourceName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Resource> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Resource(
            projectName : new SingleLineText(row['プロジェクト名']),
            sercieName : new SingleLineText(row['サービス名']),
            apiVersion : new SingleLineText(row['APIバージョン']),
            name : new SingleLineText(row['名称']),
            namespace : new JavaNamespace(row['名前空間']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            basePath : new UrlWithPlaceholder(row['ベースパス']),
            description : new MultiLineText(row['詳細']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  /**
   * 完全修飾名
   */
  @Memoized
  String getFqn() {
    namespace + (namespace.empty ? '' : '.') + className
  }

  /**
   * クラス名
   */
  @Memoized
  String getClassName() {
    identifier.upperCamelized.orElse('')
  }

  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}