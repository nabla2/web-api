package nabla2.webapi.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.webapi.trait.VariableTrait
import nabla2.webapi.model.Project
import nabla2.webapi.model.Operation
import nabla2.webapi.model.VariableType
/**
 * 変数
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Variable implements VariableTrait<Variable> {

  /** 論理名 */
  static final String ENTITY_NAME = "変数"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * サービス名
   */
  SingleLineText serviceName
  /**
   * APIバージョン
   */
  SingleLineText apiVersion
  /**
   * 操作名
   */
  SingleLineText operationName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * 型
   */
  ParameterizedText typeDefinition
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * プロジェクト
   */
  @Memoized
  @JsonIgnore
  Project getProject() {
    Project.from(_table$).filter {
      this.projectName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * REST操作
   */
  @Memoized
  @JsonIgnore
  Operation getRestOperation() {
    Operation.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.operationName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * 型
   */
  @Memoized
  @JsonIgnore
  VariableType getType() {
    VariableType.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.typeDefinition.sameAs(it.typeDefinition)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Variable> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Variable(
            projectName : new SingleLineText(row['プロジェクト名']),
            serviceName : new SingleLineText(row['サービス名']),
            apiVersion : new SingleLineText(row['APIバージョン']),
            operationName : new SingleLineText(row['操作名']),
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            typeDefinition : new ParameterizedText(row['型']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}