package nabla2.webapi.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * プロジェクトバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class ProjectValidator implements Validator<Project> {
  Observable<ConstraintViolation<Project>> validate(Observable<Table> table$) {
    Observable<Project> entity$ = Project.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Project>> validateUniqueConstraint(Observable<Project> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      Project another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<Project> duplicated = it.get('__duplicated__')
      new ConstraintViolation<Project>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          '名称',
          'プロジェクト',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<Project>> validateCardinalityConstraint(Observable<Project> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.systemMessage == null) {
        violations.add(new ConstraintViolation<Project>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '多言語化テキスト区分',
            "名称=${entity.systemMessageDefinition}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Project>> validatePropertyType(Observable<Project> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.namespace.messageIfInvalid.map{['名前空間', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.systemMessageDefinition.messageIfInvalid.map{['システムメッセージ定義', it]}.orElse(null),
        entity.basePath.messageIfInvalid.map{['ベースパス', it]}.orElse(null),
        entity.charactersAllowedToInput.messageIfInvalid.map{['入力可能文字', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Project>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Project>> validateRequiredProperty(Observable<Project> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.name.literal.map{''}.orElse('名称'),
        entity.namespace.literal.map{''}.orElse('名前空間'),
        entity.identifier.literal.map{''}.orElse('識別子'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Project>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}