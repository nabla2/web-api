package nabla2.webapi.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.datatype.Url
import nabla2.metamodel.datatype.UrlWithPlaceholder
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.webapi.trait.ServiceTrait
import nabla2.webapi.model.Project
import nabla2.webapi.model.Resource
/**
 * サービス
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Service implements ServiceTrait<Service> {

  /** 論理名 */
  static final String ENTITY_NAME = "サービス"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * 名称
   */
  SingleLineText name
  /**
   * APIバージョン
   */
  SingleLineText apiVersion
  /**
   * 名前空間
   */
  JavaNamespace namespace
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * ホスト
   */
  Url host
  /**
   * ベースパス
   */
  UrlWithPlaceholder basePath
  /**
   * 接続許容ドメイン
   */
  SingleLineText allowedOrigin
  /**
   * 詳細
   */
  MultiLineText description

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * プロジェクト
   */
  @Memoized
  @JsonIgnore
  Project getProject() {
    Project.from(_table$).filter {
      this.projectName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * リソースリスト
   */
  @Memoized
  @JsonIgnore
  List<Resource> getResources() {
    Resource.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.name.sameAs(it.sercieName) &&
      this.apiVersion.sameAs(it.apiVersion)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Service> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Service(
            projectName : new SingleLineText(row['プロジェクト名']),
            name : new SingleLineText(row['名称']),
            apiVersion : new SingleLineText(row['APIバージョン']),
            namespace : new JavaNamespace(row['名前空間']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            host : new Url(row['ホスト']),
            basePath : new UrlWithPlaceholder(row['ベースパス']),
            allowedOrigin : new SingleLineText(row['接続許容ドメイン']),
            description : new MultiLineText(row['詳細']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  /**
   * 完全修飾名
   */
  @Memoized
  String getFqn() {
    namespace + (namespace.empty ? '' : '.') + className
  }

  /**
   * クラス名
   */
  @Memoized
  String getClassName() {
    identifier.upperCamelized.orElse('')
  }

  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}