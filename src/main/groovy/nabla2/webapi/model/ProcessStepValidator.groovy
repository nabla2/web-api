package nabla2.webapi.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 処理ステップバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class ProcessStepValidator implements Validator<ProcessStep> {
  Observable<ConstraintViolation<ProcessStep>> validate(Observable<Table> table$) {
    Observable<ProcessStep> entity$ = ProcessStep.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<ProcessStep>> validateUniqueConstraint(Observable<ProcessStep> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        serviceName : entity.serviceName.value.map{it.toString()}.orElse(null),
        apiVersion : entity.apiVersion.value.map{it.toString()}.orElse(null),
        operationName : entity.operationName.value.map{it.toString()}.orElse(null),
        stepOrder : entity.stepOrder.value.map{it.toString()}.orElse(null),
      ])
      ProcessStep another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<ProcessStep> duplicated = it.get('__duplicated__')
      new ConstraintViolation<ProcessStep>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, サービス名, APIバージョン, 操作名, 実行順',
          '処理ステップ',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<ProcessStep>> validateCardinalityConstraint(Observable<ProcessStep> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.project == null) {
        violations.add(new ConstraintViolation<ProcessStep>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'プロジェクト',
            "名称=${entity.projectName}",
            entity,
          )
        ))
      }
      if (entity.restOperation == null) {
        violations.add(new ConstraintViolation<ProcessStep>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '操作',
            "プロジェクト名=${entity.projectName}, サービス名=${entity.serviceName}, APIバージョン=${entity.apiVersion}, 名称=${entity.operationName}",
            entity,
          )
        ))
      }
      if (entity.definition == null) {
        violations.add(new ConstraintViolation<ProcessStep>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '処理ステップ定義',
            "プロジェクト名=${entity.projectName}, 内容=${entity.content}",
            entity,
          )
        ))
      }
      if (!entity.returnVariableName.sameAs('NO_RETURN_VALUE') &&entity.returnVariable == null) {
        violations.add(new ConstraintViolation<ProcessStep>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '変数',
            "プロジェクト名=${entity.projectName}, サービス名=${entity.serviceName}, APIバージョン=${entity.apiVersion}, 操作名=${entity.operationName}, 名称=${entity.returnVariableName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ProcessStep>> validatePropertyType(Observable<ProcessStep> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.serviceName.messageIfInvalid.map{['サービス名', it]}.orElse(null),
        entity.apiVersion.messageIfInvalid.map{['APIバージョン', it]}.orElse(null),
        entity.operationName.messageIfInvalid.map{['操作名', it]}.orElse(null),
        entity.stepOrder.messageIfInvalid.map{['実行順', it]}.orElse(null),
        entity.content.messageIfInvalid.map{['内容', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.returnVariableName.messageIfInvalid.map{['結果変数名', it]}.orElse(null),
        entity.argumentsNames.messageIfInvalid.map{['引数変数名', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<ProcessStep>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<ProcessStep>> validateRequiredProperty(Observable<ProcessStep> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.serviceName.literal.map{''}.orElse('サービス名'),
        entity.apiVersion.literal.map{''}.orElse('APIバージョン'),
        entity.operationName.literal.map{''}.orElse('操作名'),
        entity.stepOrder.literal.map{''}.orElse('実行順'),
        entity.content.literal.map{''}.orElse('内容'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.returnVariableName.literal.map{''}.orElse('結果変数名'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<ProcessStep>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}