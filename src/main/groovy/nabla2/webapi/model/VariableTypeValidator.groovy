package nabla2.webapi.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 変数型バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class VariableTypeValidator implements Validator<VariableType> {
  Observable<ConstraintViolation<VariableType>> validate(Observable<Table> table$) {
    Observable<VariableType> entity$ = VariableType.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<VariableType>> validateUniqueConstraint(Observable<VariableType> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        typeDefinition : entity.typeDefinition.value.map{it.toString()}.orElse(null),
      ])
      VariableType another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<VariableType> duplicated = it.get('__duplicated__')
      new ConstraintViolation<VariableType>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, 型定義',
          '変数型',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<VariableType>> validateCardinalityConstraint(Observable<VariableType> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.project == null) {
        violations.add(new ConstraintViolation<VariableType>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'プロジェクト',
            "名称=${entity.projectName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<VariableType>> validatePropertyType(Observable<VariableType> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.typeDefinition.messageIfInvalid.map{['型定義', it]}.orElse(null),
        entity.templateNamespace.messageIfInvalid.map{['テンプレート名前空間', it]}.orElse(null),
        entity.templateIdentifier.messageIfInvalid.map{['テンプレート識別子', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<VariableType>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<VariableType>> validateRequiredProperty(Observable<VariableType> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.typeDefinition.literal.map{''}.orElse('型定義'),
        entity.templateNamespace.literal.map{''}.orElse('テンプレート名前空間'),
        entity.templateIdentifier.literal.map{''}.orElse('テンプレート識別子'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<VariableType>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}