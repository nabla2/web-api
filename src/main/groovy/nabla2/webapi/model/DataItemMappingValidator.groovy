package nabla2.webapi.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * データ項目マッピングバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class DataItemMappingValidator implements Validator<DataItemMapping> {
  Observable<ConstraintViolation<DataItemMapping>> validate(Observable<Table> table$) {
    Observable<DataItemMapping> entity$ = DataItemMapping.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<DataItemMapping>> validateUniqueConstraint(Observable<DataItemMapping> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        serviceName : entity.serviceName.value.map{it.toString()}.orElse(null),
        apiVersion : entity.apiVersion.value.map{it.toString()}.orElse(null),
        operationName : entity.operationName.value.map{it.toString()}.orElse(null),
        location : entity.location.value.map{it.toString()}.orElse(null),
        parentDataItemName : entity.parentDataItemName.value.map{it.toString()}.orElse(null),
        dataItemName : entity.dataItemName.value.map{it.toString()}.orElse(null),
      ])
      DataItemMapping another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<DataItemMapping> duplicated = it.get('__duplicated__')
      new ConstraintViolation<DataItemMapping>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, サービス名, APIバージョン, 操作名, 区分, データ親項目名, データ項目名',
          'データ項目マッピング',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<DataItemMapping>> validateCardinalityConstraint(Observable<DataItemMapping> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.operation == null) {
        violations.add(new ConstraintViolation<DataItemMapping>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '操作',
            "プロジェクト名=${entity.projectName}, サービス名=${entity.serviceName}, APIバージョン=${entity.apiVersion}, 名称=${entity.operationName}",
            entity,
          )
        ))
      }
      if (entity.dataItem == null) {
        violations.add(new ConstraintViolation<DataItemMapping>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'データ項目',
            "プロジェクト名=${entity.projectName}, サービス名=${entity.serviceName}, APIバージョン=${entity.apiVersion}, 操作名=${entity.operationName}, 区分=${entity.location}, 親項目名=${entity.parentDataItemName}, 名称=${entity.dataItemName}",
            entity,
          )
        ))
      }
      if (entity.targetVariable == null) {
        violations.add(new ConstraintViolation<DataItemMapping>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '変数',
            "プロジェクト名=${entity.projectName}, サービス名=${entity.serviceName}, APIバージョン=${entity.apiVersion}, 操作名=${entity.operationName}, 名称=${entity.targetVariableName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<DataItemMapping>> validatePropertyType(Observable<DataItemMapping> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.serviceName.messageIfInvalid.map{['サービス名', it]}.orElse(null),
        entity.apiVersion.messageIfInvalid.map{['APIバージョン', it]}.orElse(null),
        entity.operationName.messageIfInvalid.map{['操作名', it]}.orElse(null),
        entity.location.messageIfInvalid.map{['区分', it]}.orElse(null),
        entity.parentDataItemName.messageIfInvalid.map{['データ親項目名', it]}.orElse(null),
        entity.dataItemName.messageIfInvalid.map{['データ項目名', it]}.orElse(null),
        entity.targetVariableName.messageIfInvalid.map{['対象変数名', it]}.orElse(null),
        entity.targetItemName.messageIfInvalid.map{['対象項目名', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<DataItemMapping>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<DataItemMapping>> validateRequiredProperty(Observable<DataItemMapping> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.serviceName.literal.map{''}.orElse('サービス名'),
        entity.apiVersion.literal.map{''}.orElse('APIバージョン'),
        entity.operationName.literal.map{''}.orElse('操作名'),
        entity.location.literal.map{''}.orElse('区分'),
        entity.parentDataItemName.literal.map{''}.orElse('データ親項目名'),
        entity.dataItemName.literal.map{''}.orElse('データ項目名'),
        entity.targetVariableName.literal.map{''}.orElse('対象変数名'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<DataItemMapping>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}