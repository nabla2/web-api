package nabla2.webapi.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * サービスバリデータ
 *
 * @author nabla2.metamodel.generator
 */
class ServiceValidator implements Validator<Service> {
  Observable<ConstraintViolation<Service>> validate(Observable<Table> table$) {
    Observable<Service> entity$ = Service.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Service>> validateUniqueConstraint(Observable<Service> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
        apiVersion : entity.apiVersion.value.map{it.toString()}.orElse(null),
      ])
      Service another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<Service> duplicated = it.get('__duplicated__')
      new ConstraintViolation<Service>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, 名称, APIバージョン',
          'サービス',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<Service>> validateCardinalityConstraint(Observable<Service> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.project == null) {
        violations.add(new ConstraintViolation<Service>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'プロジェクト',
            "名称=${entity.projectName}",
            entity,
          )
        ))
      }
      if (entity.resources == null) {
        violations.add(new ConstraintViolation<Service>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'リソース',
            "プロジェクト名=${entity.projectName}, サービス名=${entity.name}, APIバージョン=${entity.apiVersion}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Service>> validatePropertyType(Observable<Service> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.apiVersion.messageIfInvalid.map{['APIバージョン', it]}.orElse(null),
        entity.namespace.messageIfInvalid.map{['名前空間', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.host.messageIfInvalid.map{['ホスト', it]}.orElse(null),
        entity.basePath.messageIfInvalid.map{['ベースパス', it]}.orElse(null),
        entity.allowedOrigin.messageIfInvalid.map{['接続許容ドメイン', it]}.orElse(null),
        entity.description.messageIfInvalid.map{['詳細', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Service>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Service>> validateRequiredProperty(Observable<Service> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.apiVersion.literal.map{''}.orElse('APIバージョン'),
        entity.namespace.literal.map{''}.orElse('名前空間'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.basePath.literal.map{''}.orElse('ベースパス'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Service>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}