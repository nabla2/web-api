package nabla2.webapi.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.datatype.UrlWithPlaceholder
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.webapi.trait.OperationTrait
import nabla2.webapi.model.Resource
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.ProcessStep
import nabla2.webapi.model.Variable
import nabla2.webapi.model.DataItemMapping
import nabla2.webapi.model.Report
/**
 * 操作
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class Operation implements OperationTrait<Operation> {

  /** 論理名 */
  static final String ENTITY_NAME = "操作"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * サービス名
   */
  SingleLineText serviceName
  /**
   * APIバージョン
   */
  SingleLineText apiVersion
  /**
   * 名称
   */
  SingleLineText name
  /**
   * 識別子
   */
  StrictJavaIdentifier identifier
  /**
   * リソース名
   */
  SingleLineText resourceName
  /**
   * パス
   */
  UrlWithPlaceholder path
  /**
   * HTTPメソッド
   */
  SingleLineText httpMehtod
  /**
   * 詳細
   */
  MultiLineText description

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * リソース
   */
  @Memoized
  @JsonIgnore
  Resource getResource() {
    Resource.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.sercieName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.resourceName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * データ項目リスト
   */
  @Memoized
  @JsonIgnore
  List<DataItem> getDataItems() {
    DataItem.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.name.sameAs(it.operationName)
    }.toList().blockingGet()
  }
  /**
   * 処理ステップリスト
   */
  @Memoized
  @JsonIgnore
  List<ProcessStep> getProcessSteps() {
    ProcessStep.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.name.sameAs(it.operationName)
    }.toList().blockingGet()
  }
  /**
   * 処理変数
   */
  @Memoized
  @JsonIgnore
  List<Variable> getProcessVariables() {
    Variable.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.name.sameAs(it.operationName)
    }.toList().blockingGet()
  }
  /**
   * データ項目マッピングリスト
   */
  @Memoized
  @JsonIgnore
  List<DataItemMapping> getDataItemMappings() {
    DataItemMapping.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.name.sameAs(it.operationName)
    }.toList().blockingGet()
  }
  /**
   * 帳票リスト
   */
  @Memoized
  @JsonIgnore
  List<Report> getReports() {
    Report.from(_table$).filter {
      this.projectName.sameAs(it.projectName) &&
      this.serviceName.sameAs(it.serviceName) &&
      this.apiVersion.sameAs(it.apiVersion) &&
      this.name.sameAs(it.operationName)
    }.toList().blockingGet()
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<Operation> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new Operation(
            projectName : new SingleLineText(row['プロジェクト名']),
            serviceName : new SingleLineText(row['サービス名']),
            apiVersion : new SingleLineText(row['APIバージョン']),
            name : new SingleLineText(row['名称']),
            identifier : new StrictJavaIdentifier(row['識別子']),
            resourceName : new SingleLineText(row['リソース名']),
            path : new UrlWithPlaceholder(row['パス']),
            httpMehtod : new SingleLineText(row['HTTPメソッド']),
            description : new MultiLineText(row['詳細']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}