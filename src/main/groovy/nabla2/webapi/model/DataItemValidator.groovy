package nabla2.webapi.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * データ項目バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class DataItemValidator implements Validator<DataItem> {
  Observable<ConstraintViolation<DataItem>> validate(Observable<Table> table$) {
    Observable<DataItem> entity$ = DataItem.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<DataItem>> validateUniqueConstraint(Observable<DataItem> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        serviceName : entity.serviceName.value.map{it.toString()}.orElse(null),
        apiVersion : entity.apiVersion.value.map{it.toString()}.orElse(null),
        operationName : entity.operationName.value.map{it.toString()}.orElse(null),
        location : entity.location.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
        parentName : entity.parentName.value.map{it.toString()}.orElse(null),
      ])
      DataItem another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<DataItem> duplicated = it.get('__duplicated__')
      new ConstraintViolation<DataItem>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, サービス名, APIバージョン, 操作名, 区分, 名称, 親項目名',
          'データ項目',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<DataItem>> validateCardinalityConstraint(Observable<DataItem> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.operation == null) {
        violations.add(new ConstraintViolation<DataItem>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '操作',
            "プロジェクト名=${entity.projectName}, サービス名=${entity.serviceName}, APIバージョン=${entity.apiVersion}, 名称=${entity.operationName}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<DataItem>> validatePropertyType(Observable<DataItem> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.serviceName.messageIfInvalid.map{['サービス名', it]}.orElse(null),
        entity.apiVersion.messageIfInvalid.map{['APIバージョン', it]}.orElse(null),
        entity.operationName.messageIfInvalid.map{['操作名', it]}.orElse(null),
        entity.location.messageIfInvalid.map{['区分', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.parentName.messageIfInvalid.map{['親項目名', it]}.orElse(null),
        entity.type.messageIfInvalid.map{['データ型', it]}.orElse(null),
        entity.required.messageIfInvalid.map{['必須', it]}.orElse(null),
        entity.maxSize.messageIfInvalid.map{['最大長', it]}.orElse(null),
        entity.formatName.messageIfInvalid.map{['フォーマット', it]}.orElse(null),
        entity.example.messageIfInvalid.map{['例', it]}.orElse(null),
        entity.description.messageIfInvalid.map{['詳細', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<DataItem>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<DataItem>> validateRequiredProperty(Observable<DataItem> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.serviceName.literal.map{''}.orElse('サービス名'),
        entity.apiVersion.literal.map{''}.orElse('APIバージョン'),
        entity.operationName.literal.map{''}.orElse('操作名'),
        entity.location.literal.map{''}.orElse('区分'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.parentName.literal.map{''}.orElse('親項目名'),
        entity.type.literal.map{''}.orElse('データ型'),
        entity.formatName.literal.map{''}.orElse('フォーマット'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<DataItem>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}