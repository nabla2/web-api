package nabla2.webapi.model

import groovy.json.JsonOutput
import io.reactivex.Observable
import java.text.MessageFormat
import nabla2.metamodel.model.Entity
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.metamodel.model.constraint.Validator

/**
 * 変数バリデータ
 *
 * @author nabla2.metamodel.generator
 */
class VariableValidator implements Validator<Variable> {
  Observable<ConstraintViolation<Variable>> validate(Observable<Table> table$) {
    Observable<Variable> entity$ = Variable.from(table$)
    Observable.fromArray(
      validateUniqueConstraint(entity$),
      validateCardinalityConstraint(entity$),
      validatePropertyType(entity$),
      validateRequiredProperty(entity$),
    )
    .flatMap() { it }
  }

  static Observable<ConstraintViolation<Variable>> validateUniqueConstraint(Observable<Variable> entity$) {
    entity$.scan([:]) { entityOf, entity ->
      String key = JsonOutput.toJson([
        projectName : entity.projectName.value.map{it.toString()}.orElse(null),
        serviceName : entity.serviceName.value.map{it.toString()}.orElse(null),
        apiVersion : entity.apiVersion.value.map{it.toString()}.orElse(null),
        operationName : entity.operationName.value.map{it.toString()}.orElse(null),
        name : entity.name.value.map{it.toString()}.orElse(null),
      ])
      Variable another = entityOf[key]
      entityOf[key] = entity
      entityOf['__duplicated__'] = another ? [another, entity] : null
      entityOf
    }.filter {
      it.get('__duplicated__') != null
    }.map {
      List<Variable> duplicated = it.get('__duplicated__')
      new ConstraintViolation<Variable>(
        causedBy : duplicated,
        message : MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('duplicated_entities'),
          'プロジェクト名, サービス名, APIバージョン, 操作名, 名称',
          '変数',
          duplicated[0],
          duplicated[1]
        )
      )
    }
  }

  static Observable<ConstraintViolation<Variable>> validateCardinalityConstraint(Observable<Variable> entity$) {
    entity$.map { entity ->
      List<ConstraintViolation> violations = []
      if (entity.project == null) {
        violations.add(new ConstraintViolation<Variable>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            'プロジェクト',
            "名称=${entity.projectName}",
            entity,
          )
        ))
      }
      if (entity.restOperation == null) {
        violations.add(new ConstraintViolation<Variable>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '操作',
            "プロジェクト名=${entity.projectName}, サービス名=${entity.serviceName}, APIバージョン=${entity.apiVersion}, 名称=${entity.operationName}",
            entity,
          )
        ))
      }
      if (entity.type == null) {
        violations.add(new ConstraintViolation<Variable>(
          causedBy: [entity],
          message : MessageFormat.format(
            ResourceBundle.getBundle('metamodel_messages').getString('missing_reference'),
            '変数型',
            "プロジェクト名=${entity.projectName}, 型定義=${entity.typeDefinition}",
            entity,
          )
        ))
      }
      Observable.fromIterable(violations)
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Variable>> validatePropertyType(Observable<Variable> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.messageIfInvalid.map{['プロジェクト名', it]}.orElse(null),
        entity.serviceName.messageIfInvalid.map{['サービス名', it]}.orElse(null),
        entity.apiVersion.messageIfInvalid.map{['APIバージョン', it]}.orElse(null),
        entity.operationName.messageIfInvalid.map{['操作名', it]}.orElse(null),
        entity.name.messageIfInvalid.map{['名称', it]}.orElse(null),
        entity.identifier.messageIfInvalid.map{['識別子', it]}.orElse(null),
        entity.typeDefinition.messageIfInvalid.map{['型', it]}.orElse(null),
        entity.comment.messageIfInvalid.map{['コメント', it]}.orElse(null),
      ]
      .findAll()
      .collect { m -> new ConstraintViolation<Variable>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('invalid_datatype'),
          m[0],
          m[1],
          entity
        )
      )})
    }
    .flatMap() {it}
  }

  static Observable<ConstraintViolation<Variable>> validateRequiredProperty(Observable<Variable> entity$) {
    entity$.map { entity ->
      Observable.fromIterable([
        entity.projectName.literal.map{''}.orElse('プロジェクト名'),
        entity.serviceName.literal.map{''}.orElse('サービス名'),
        entity.apiVersion.literal.map{''}.orElse('APIバージョン'),
        entity.operationName.literal.map{''}.orElse('操作名'),
        entity.name.literal.map{''}.orElse('名称'),
        entity.identifier.literal.map{''}.orElse('識別子'),
        entity.typeDefinition.literal.map{''}.orElse('型'),
      ]
      .findAll{!it.empty}
      .collect { m -> new ConstraintViolation<Variable>(
        causedBy: [entity],
        message: MessageFormat.format(
          ResourceBundle.getBundle('metamodel_messages').getString('missing_value'),
          m,
          entity
        )
      )})
    }
    .flatMap() {it}
  }
}