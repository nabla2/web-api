package nabla2.webapi.model

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.Memoized
import io.reactivex.Observable
import nabla2.metamodel.datatype.JavaNamespace
import nabla2.metamodel.datatype.MultiLineText
import nabla2.metamodel.datatype.ParameterizedText
import nabla2.metamodel.datatype.SingleLineText
import nabla2.metamodel.datatype.StrictJavaIdentifier
import nabla2.metamodel.model.Row
import nabla2.metamodel.model.Table
import nabla2.webapi.trait.VariableTypeTrait
import nabla2.webapi.model.Project
/**
 * 変数型
 * @author nabla2.metamodel.generator
 */
@Canonical(excludes = ['_table$', '_table', '_rowIndex'])
class VariableType implements VariableTypeTrait<VariableType> {

  /** 論理名 */
  static final String ENTITY_NAME = "変数型"

  // ----- プロパティ定義 ------ //

  /**
   * プロジェクト名
   */
  SingleLineText projectName
  /**
   * 型定義
   */
  ParameterizedText typeDefinition
  /**
   * テンプレート名前空間
   */
  JavaNamespace templateNamespace
  /**
   * テンプレート識別子
   */
  StrictJavaIdentifier templateIdentifier
  /**
   * コメント
   */
  MultiLineText comment

  // ----- 定義元情報 ------ //

  private transient Observable<Table> _table$

  private Table _table

  Row _row

  // ----- メソッド定義 ------ //

  /**
   * プロジェクト
   */
  @Memoized
  @JsonIgnore
  Project getProject() {
    Project.from(_table$).filter {
      this.projectName.sameAs(it.name)
    }.blockingFirst(null)
  }
  /**
   * テーブルストリームからエンティティストリームを生成する。
   */
  @Memoized
  static Observable<VariableType> from(Observable<Table> table$) {
    table$
    .filter{
      it.name.content == Table.OPEN_BRACE_MARK +
                         ENTITY_NAME +
                         Table.CLOSE_BRACE_MARK
    }
    .map { table ->
      Observable.fromIterable(
        table.rowsAsMap.withIndex().collect { Map<String, String> row, int i ->
          new VariableType(
            projectName : new SingleLineText(row['プロジェクト名']),
            typeDefinition : new ParameterizedText(row['型定義']),
            templateNamespace : new JavaNamespace(row['テンプレート名前空間']),
            templateIdentifier : new StrictJavaIdentifier(row['テンプレート識別子']),
            comment : new MultiLineText(row['コメント']),
            _table$ : table$,
            _table : table,
            _row : table.rows[i],
          )
        }
      )
    }
    .flatMap {it}
    .toList().blockingGet().with{ Observable.fromIterable(it)}
  }
  @Override
  String toString() {
    _table.filter{ it.rowIndex == _row.rowIndex }.toString()
  }
}