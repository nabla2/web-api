package nabla2.webapi

import groovy.transform.Canonical
import io.reactivex.Observable
import nabla2.excel.ExcelWorkbookLoader
import nabla2.metamodel.model.Table
import nabla2.metamodel.model.constraint.ConstraintViolation
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.DataItemMapping
import nabla2.webapi.model.DataItemMappingValidator
import nabla2.webapi.model.DataItemValidator
import nabla2.webapi.model.FieldValidator
import nabla2.webapi.model.FieldValidatorValidator
import nabla2.webapi.model.Format
import nabla2.webapi.model.FormatValidator
import nabla2.webapi.model.Operation
import nabla2.webapi.model.OperationValidator
import nabla2.webapi.model.ProcessStep
import nabla2.webapi.model.ProcessStepDefinition
import nabla2.webapi.model.ProcessStepDefinitionValidator
import nabla2.webapi.model.ProcessStepValidator
import nabla2.webapi.model.Project
import nabla2.webapi.model.ProjectValidator
import nabla2.webapi.model.Report
import nabla2.webapi.model.ReportValidator
import nabla2.webapi.model.Resource
import nabla2.webapi.model.ResourceValidator
import nabla2.webapi.model.Service
import nabla2.webapi.model.ServiceValidator
import nabla2.webapi.model.Variable
import nabla2.webapi.model.VariableType
import nabla2.webapi.model.VariableTypeValidator
import nabla2.webapi.model.VariableValidator

/**
 * WebAPIエンティティローダ
 *
 * @author nabla2.metamodel.generator
 */
@Canonical
class WebapiEntityLoader {

  final Observable<Table> table$

  private WebapiEntityLoader(File file) {
    table$ = ExcelWorkbookLoader.from(file).table$
  }

  private WebapiEntityLoader(InputStream stream) {
    table$ = ExcelWorkbookLoader.from(stream).table$
  }

  static WebapiEntityLoader from(File file) {
    new WebapiEntityLoader(file)
  }

  static WebapiEntityLoader from(InputStream stream) {
    new WebapiEntityLoader(stream)
  }

  Observable<Project> getProject$() {
    Project.from(table$)
  }
  Observable<Service> getService$() {
    Service.from(table$)
  }
  Observable<Resource> getResource$() {
    Resource.from(table$)
  }
  Observable<Operation> getOperation$() {
    Operation.from(table$)
  }
  Observable<Report> getReport$() {
    Report.from(table$)
  }
  Observable<DataItem> getDataItem$() {
    DataItem.from(table$)
  }
  Observable<Format> getFormat$() {
    Format.from(table$)
  }
  Observable<FieldValidator> getFieldValidator$() {
    FieldValidator.from(table$)
  }
  Observable<DataItemMapping> getDataItemMapping$() {
    DataItemMapping.from(table$)
  }
  Observable<ProcessStep> getProcessStep$() {
    ProcessStep.from(table$)
  }
  Observable<Variable> getVariable$() {
    Variable.from(table$)
  }
  Observable<VariableType> getVariableType$() {
    VariableType.from(table$)
  }
  Observable<ProcessStepDefinition> getProcessStepDefinition$() {
    ProcessStepDefinition.from(table$)
  }

  Observable<ConstraintViolation<?>> getConstraintViolation$() {
    Observable.fromArray(
      new ProjectValidator().validate(table$),
      new ServiceValidator().validate(table$),
      new ResourceValidator().validate(table$),
      new OperationValidator().validate(table$),
      new ReportValidator().validate(table$),
      new DataItemValidator().validate(table$),
      new FormatValidator().validate(table$),
      new FieldValidatorValidator().validate(table$),
      new DataItemMappingValidator().validate(table$),
      new ProcessStepValidator().validate(table$),
      new VariableValidator().validate(table$),
      new VariableTypeValidator().validate(table$),
      new ProcessStepDefinitionValidator().validate(table$),
    ).flatMap{ it }
  }

}