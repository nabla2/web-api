package nabla2.webapi.template

import groovy.json.StringEscapeUtils
import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.FieldValidator
import nabla2.webapi.model.Operation
import nabla2.webapi.model.Project
import nabla2.webapi.model.Resource

@Canonical
class RestRequestPayloadJava implements TextFileTemplate {

  Operation operation

  Resource getResource() {
    operation.resource
  }

  String getClassName() {
    operation.identifier.upperCamelized.get() + 'Request'
  }

  String getName() {
    operation.identifier.lowerCamelized.get()
  }

  List<DataItem> getDataItems() {
    operation.dataItems.findAll{ it.location.get().contains('リクエスト')}
  }

  @Memoized
  String getSource() {
  """\
  |package ${operation.namespace};
  |
  ${modules.collect{"""\
  |import ${it};
  """.trim()}.join('\n')}
  |import static ${project.namespace.get()}.${SystemMessageJava.className}.*;
  |import static ${resource.service.project.namespace.get()}.CommonUtils.*;
  |
  |/**
  | * ${operation.name}のリクエストデータを格納するJavaBeanクラス
  | *
  | * ${regenerationMark}
  | */
  |public class ${className} {
  ${dataItems.findAll{!it.parent}.collect{ accessorFor(it) }.join('\n')}
  ${dataItems.findAll{it.type.sameAs('object')}.collect{
    innerBeanClassOf(it)
  }.join('\n')}
  |}
  """.replaceAll(/\n\s*\n/, '\n').stripMargin().replaceAll(/\n\s*\n/, '\n\n').trim()
  }

  static String accessorFor(DataItem item) {
    String uName = item.identifier.upperCamelized.get()
    String lName = item.identifier.lowerCamelized.get()

    switch (item.type) {

    case '[key]':
    case 'string': return """\
    |    /**
    |     * ${item.name}
    |     */
    ${annotationsFor(item).with{it.empty ? '' :it.collect{"""
    |    ${it}
    """.trim()}.join('\n')}}
    |    private String ${lName};
    |
    |    /**
    |     * ${item.name}を返す。
    |     * 末尾の空白文字はトリムされる。
    |     * また未設定の場合は{@code null}を返す。
    |     * @return ${item.name}
    |     */
    |    public String get${uName}() {
    |        return normalize(this.${lName});
    |    }
    |
    |    /**
    |     * ${item.name}を設定する。
    |     * @param ${lName} ${item.name}
    |     */
    |    public void set${uName}(String ${lName}) {
    |        this.${lName} = normalize(${lName});
    |    }
    """.trim()

    case 'boolean': return """\
    |    /**
    |     * ${item.name}
    |     */
    ${annotationsFor(item).with{it.empty ? '' :it.collect{"""
    |    ${it}
    """.trim()}.join('\n')}}
    |    private Boolean ${lName};
    |
    |    /**
    |     * ${item.name}を返す。
    |     * 未設定の場合は{@code null}を返す。
    |     * @return ${item.name}
    |     */
    |    public Boolean get${uName}() {
    |        return nullSafe(this.${lName});
    |    }
    |
    |    /**
    |     * ${item.name}を返す。
    |     * 値が未設定であった場合は{@code false}を返す。
    |     * @return ${item.name}
    |     */
    |    public boolean is${uName}() {
    |        return nullSafe(this.${lName});
    |    }
    |
    |    /**
    |     * ${item.name}を設定する。
    |     * @param ${lName} ${item.name}
    |     */
    |    public void set${uName}(Boolean ${lName}) {
    |        this.${lName} = ${lName};
    |    }
    """.trim()


    case 'number' : return """\
    |    /**
    |     * ${item.name}
    |     */
    ${annotationsFor(item).with{it.empty ? '' :it.collect{"""
    |    ${it}
    """.trim()}.join('\n')}}
    |    private BigDecimal ${lName};
    |
    |    /**
    |     * ${item.name}を返す。
    |     * 未設定の場合は{@code null}を返す。
    |     * @return ${item.name}
    |     */
    |    public BigDecimal get${uName}() {
    |        return nullSafe(this.${lName});
    |    }
    |
    |    /**
    |     * ${item.name}を整数として返す。
    |     * 未設定の場合は{@code 0}を返す。
    |     * 少数部分は切り捨てられる。
    |     * @return ${item.name}
    |     */
    |    public int get${uName}AsInt() {
    |        return nullSafe(this.${lName}).intValue();
    |    }
    |
    |    /**
    |     * ${item.name}を設定する。
    |     * @param ${lName} ${item.name}
    |     */
    |    public void set${uName}(BigDecimal ${lName}) {
    |        this.${lName} = ${lName};
    |    }
    |
    |    /**
    |     * ${item.name}を設定する。
    |     * @param ${lName} ${item.name}
    |     */
    |    public void set${uName}AsInt(int ${lName}) {
    |        this.${lName} = BigDecimal.valueOf(${lName});
    |    }
    """.trim()

    case 'object' : return """\
    |    /**
    |     * ${item.name}
    |     */
    |    @Valid
    |    private ${uName} ${lName};
    |
    |    /**
    |     * ${item.name}を返す。
    |     * @return ${item.name}
    |     */
    |    public ${uName} get${uName}() {
    |        return this.${lName};
    |    }
    |
    |    /**
    |     * ${item.name}を設定する。
    |     */
    |    public void set${uName}(${uName} ${lName}) {
    |       this.${lName} = ${lName};
    |    }
    """.trim()

    case 'array' : return item.children.first().with { child ->
      if (!child) throw new IllegalStateException(
      "any array props must have at least one child item: ${item.name}"
      )
      String arrayType = child.type.sameAs('string')  ? 'String'  :
                         child.type.sameAs('boolean') ? 'Boolean' :
                         child.type.sameAs('number' ) ? 'BigDecimal' :
                         child.type.sameAs('object' ) ? child.identifier.className.get() :
                         'Object'
    """\
    |    /**
    |     * ${item.name}
    |     */
    |    @Valid
    |    private List<${arrayType}> ${lName} = new ArrayList<>();
    |
    |    /**
    |     * ${item.name}を返す。
    |     * @return ${item.name}
    |     */
    |    public List<${arrayType}> get${uName}() {
    |        return this.${lName};
    |    }
    |
    |    /**
    |     * ${item.name}を設定する。
    |     */
    |    public void set${uName}(List<${arrayType}> ${lName}) {
    |        this.${lName} = (${lName} == null)
    |                      ? new ArrayList<>()
    |                      : ${lName};
    |    }
    """.trim()}

    }

    throw new IllegalStateException(
      "Unsupported json type :${item.type}\n${item}"
    )
  }

  static String innerBeanClassOf(DataItem item)  {
    String className = item.identifier.upperCamelized.get()
  """\
  |    public static class ${className} {
  ${item.children.collect{accessorFor(it)}.join('\n').replaceAll(/((^|\n)\s*)\|/, '$1|    ')}
  |    }
  """
  }

  @Memoized
  static List<String> annotationsFor(DataItem item) {
    List<String> results = []

    if (item.required.truthy && !item.location.sameAs('リクエストパス')) {
      results.addAll(
        '@NotNull(message = "{" + FIELD_REQUIRED + "}")',
        '@NotEmpty(message = "{" + FIELD_REQUIRED + "}")'
      )
    }

    if (!item.maxSize.empty) {
      int size = item.maxSize.get()
      results.add(
        """@Size(max=${size}, message="{" + FIELD_SIZE_MAX + "}")""".toString()
      )
    }

    if (item.format) {
      FieldValidator validator = item.format.validator
      Map<String, String> kargs = validator.name.captureOptions(item.format.validatorDefinition.literal.get())
      kargs.putAll(item.format.name.captureOptions(item.formatName.get()))
      String opts = kargs.collect{key, val ->
          (key == 'message') ? "message=\"{\" + ${val.replace('.', '_').toUpperCase()} + \"}\""
                             : "${key}=\"${StringEscapeUtils.escapeJava(val)}\""
      }.join(', ')
      results.add(
      "@${validator.identifier.className.get()}(${opts})"
      )
    }

    if (item.type.sameAs('string')) {
      results.add(
        '@Pattern(regexp=ALLOWED_CHARS, message="{" + FIELD_INVALID_CHARS + "}")'
      )
    }
    results
  }

  List<String> getModules() {
    ([ usesNumberType() ? 'java.math.BigDecimal' : null,
      'org.hibernate.validator.constraints.NotEmpty',
      'java.util.List',
      'java.util.ArrayList',
      'javax.validation.Valid',
      'javax.validation.constraints.Size',
      'javax.validation.constraints.NotNull',
      'javax.validation.constraints.Pattern',
    ] + project.fieldValidators.collect{
      "${project.namespace}.validator.${it.identifier.className.get()}"
    })
    .findAll {it}
    .sort()
    .unique()
  }

  Project getProject() {
    resource.service.project
  }

  boolean usesNumberType() {
    resource.operations*.dataItems.flatten().any {
      it.type.sameAs('number')
    }
  }

  @Override
  String getRelPath() {
    "${operation.namespace.replace('.', '/')}/${className}.java"
  }
}
