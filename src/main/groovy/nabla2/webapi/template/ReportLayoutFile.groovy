package nabla2.webapi.template

import groovy.transform.Canonical
import nabla2.generator.FileTemplate
import nabla2.webapi.gradle.GenerateRestApiServiceOptionSet
import nabla2.webapi.model.Report

@Canonical
class ReportLayoutFile implements FileTemplate  {

  Report report

  GenerateRestApiServiceOptionSet options

  String regenerationMark = 'N/A'

  @Override
  boolean needsRegenerated(File file) {
    true
  }

  @Override
  InputStream getResourceAsStream() {
    new FileInputStream(report.reportsLayoutFile)
  }

  @Override
  String getRelPath() {
    report.layoutFilePath
  }
}
