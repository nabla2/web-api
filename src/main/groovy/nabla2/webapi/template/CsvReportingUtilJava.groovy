package nabla2.webapi.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.webapi.gradle.GenerateRestApiServiceOptionSet
import nabla2.webapi.model.Project

@Canonical
class CsvReportingUtilJava implements TextFileTemplate {

  Project project

  GenerateRestApiServiceOptionSet options

  @Override
  String getSource() {
    """\
    |package ${project.namespace.get()};
    |
    ${modules.collect{"""\
    |import ${it};
    """.trim()}.join('\n')}
    |/**
    | * CSV帳票処理用ユーティリティクラス
    | * ${regenerationMark}
    | */
    |public class ${className} {
    |    /**
    |     * CSVダウンロードを実行する。
    |     *
    |     * @param settingsFile CSVダウンロード設定ファイル(JSON形式)
    |     * @param vars コンテキスト変数
    |     * @param filename ダウンロードファイル名
    |     * @param response レスポンス
    |     * @throws IOException
    |     *     設定ファイルの読み込み等致命的なIOエラーが発生した場合に送出される。
    |     *     (client socketのクローズ等によるIOエラーは無視する。)
    |     */
    |    public static void download(File settingsFile, Map<String, Object> vars, String filename, HttpServletResponse response) throws IOException {
    |        CsvReportingUtil.Settings settings = new ObjectMapper().readValue(settingsFile, Settings.class);
    |        String encoding = settings.getEncoding();
    |        String encodedFilename = URLEncoder.encode(filename, encoding);
    |        response.setCharacterEncoding(encoding);
    |        response.setContentType("text/csv; charset=" + encoding + ";");
    |        response.setHeader("Content-disposition", "attachment; filename=" + encodedFilename);
    |        PrintWriter writer = response.getWriter();
    |        JexlEngine jexl =  new JexlEngine();
    |        JexlContext context = new MapContext(vars);
    |        Object items = jexl.createExpression(settings.getRecords()).evaluate(context);
    |        Iterable<Object> iterator = (items instanceof Object[])
    |                                  ? Arrays.asList((Object[])items)
    |                                  : (Iterable) items;
    |
    |        CSVPrinter printer = new CSVPrinter(writer, CSVFormat.EXCEL);
    |        AtomicInteger index = new AtomicInteger(0);
    |        List<Expression> colexprs = settings.getColumns().stream()
    |                                   .map(c -> jexl.createExpression(c.getPath())).collect(Collectors.toList());
    |        iterator.forEach(item -> {
    |            context.set("each", item);
    |            context.set("index", index.get());
    |            Object[] record = colexprs.stream().map(expr -> expr.evaluate(context)).toArray();
    |            try {
    |                printer.printRecord(record);
    |            }
    |            catch (IOException e) {
    |               //NOP
    |            }
    |            index.compareAndSet(index.get(), index.get()+1);
    |        });
    |        response.flushBuffer();
    |    }
    |
    |    /**
    |     * アップロードされたCSVファイルをパースしてリクエストペイロードにマッピングする。
    |     *
    |     * @param settingsFile CSVアップロード設定ファイル(JSON形式)
    |     * @param vars コンテキスト変数
    |     * @param uploadedFile アップロードファイル
    |     * @throws IOException
    |     *     設定ファイルの読み込み等致命的なIOエラーが発生した場合に送出される。
    |     *     (client socketのクローズ等によるIOエラーは無視する。)
    |     */
    |    public static void acceptUploadFile(File settingsFile, Map<String, Object> vars, MultipartFile uploadedFile) throws IOException {
    |        Settings settings = new ObjectMapper().readValue(settingsFile, Settings.class);
    |        JexlEngine jexl =  new JexlEngine();
    |        JexlContext context = new MapContext(vars);
    |        try (InputStream in = uploadedFile.getInputStream()) {
    |            Reader reader = new InputStreamReader(in, settings.getEncoding());
    |            List<CSVRecord> csv = CSVFormat.EXCEL.parse(reader).getRecords();
    |            List<Column> cols = settings.getColumns();
    |            List<Expression> colexprs = cols.stream()
    |                            .map(c -> jexl.createExpression(c.getPath() + " = \$\$value"))
    |                            .collect(Collectors.toList());
    |            AtomicInteger index = new AtomicInteger(0);
    |            List<Object> items = csv.stream().map(record -> {
    |                Object item = settings.createNewRecord();
    |                context.set("index", index);
    |                context.set("each", item);
    |                for (int i=0; i < colexprs.size(); i++) {
    |                    Expression expr = colexprs.get(i);
    |                    Object value = (cols.get(i).getType().equals("number"))
    |                                 ? new BigDecimal(record.get(i))
    |                                 : record.get(i);
    |                    context.set("\$\$value", value);
    |                    expr.evaluate(context);
    |                }
    |                index.compareAndSet(index.get(), index.get() +1);
    |                return item;
    |            }).collect(Collectors.toList());
    |            context.set("\$\$items", items);
    |            jexl.createExpression(settings.getRecords() + " = \$\$items").evaluate(context);
    |        }
    |        catch (IOException e) {
    |            // NOP
    |        }
    |    }
    |
    |    public static class Settings {
    |        private String encoding;
    |        private Boolean withHeader;
    |        private String records;
    |        private String recordType;
    |        private List<Column> columns;
    |
    |        public Object createNewRecord() {
    |            try {
    |                return Class.forName(recordType).newInstance();
    |            }
    |            catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
    |                throw new IllegalStateException("Failed to create record object: " + recordType, e);
    |            }
    |        }
    |
    |        public String getEncoding() {
    |            return encoding;
    |        }
    |
    |        public void setEncoding(String encoding) {
    |            this.encoding = encoding;
    |        }
    |
    |        public Boolean getWithHeader() {
    |            return withHeader;
    |        }
    |
    |        public void setWithHeader(Boolean withHeader) {
    |            this.withHeader = withHeader;
    |        }
    |
    |        public String getRecords() {
    |            return records;
    |        }
    |
    |        public void setRecords(String records) {
    |            this.records = records;
    |        }
    |
    |        public List<Column> getColumns() {
    |            return columns;
    |        }
    |
    |        public void setColumns(List<Column> columns) {
    |            this.columns = columns;
    |        }
    |
    |        public String getRecordType() {
    |            return recordType;
    |        }
    |
    |        public void setRecordType(String recordType) {
    |            this.recordType = recordType;
    |        }
    |    }
    |    public static class Column {
    |        private String header;
    |        private String path;
    |        private String type = "string";
    |
    |        public String getHeader() {
    |            return header;
    |        }
    |
    |        public void setHeader(String header) {
    |            this.header = header;
    |        }
    |
    |        public String getPath() {
    |            return path;
    |        }
    |
    |        public void setPath(String path) {
    |            this.path = path;
    |        }
    |
    |        public String getType() {
    |            return type;
    |        }
    |
    |        public void setType(String type) {
    |            this.type = type;
    |        }
    |    }
    |}
    """.stripMargin().trim()
  }

  static List<String> getModules() {
    [ 'com.fasterxml.jackson.databind.ObjectMapper',
      'org.apache.commons.csv.CSVFormat',
      'org.apache.commons.csv.CSVPrinter',
      'org.apache.commons.csv.CSVRecord',
      'org.apache.commons.jexl2.Expression',
      'org.apache.commons.jexl2.JexlContext',
      'org.apache.commons.jexl2.JexlEngine',
      'org.apache.commons.jexl2.MapContext',
      'javax.servlet.http.HttpServletResponse',
      'java.net.URLEncoder',
      'java.math.BigDecimal',
      'java.util.*',
      'java.io.*',
      'java.util.concurrent.atomic.AtomicInteger',
      'java.util.stream.Collectors',
      'org.springframework.web.multipart.MultipartFile',
    ].sort().unique()
  }

  static String getClassName() {
    'CsvReportingUtil'
  }

  @Memoized
  String buildOption(String key) {
    options.restServiceGeneratorVariables.getOrDefault(key, '')
  }

  boolean isReportingFunctionUsed() {
    buildOption('CSV帳票機能の使用').toLowerCase() == 'y'
  }

  @Override
  boolean needsRegenerated(File file) {
    if (!reportingFunctionUsed) {
      return false
    }
    TextFileTemplate.super.needsRegenerated(file)
  }

  @Override
  String getRelPath() {
    "${project.namespace.get().replace('.', '/')}/${className}.java"
  }
}
