package nabla2.webapi.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.webapi.gradle.GenerateRestApiServiceOptionSet
import nabla2.webapi.model.Project

@Canonical
class BuildGradle implements TextFileTemplate {

  Project project

  GenerateRestApiServiceOptionSet options

  @Override
  String getSource() {
     """\
    |/**
    | * ${project.name} RESTサービスAPIビルドファイル
    | * ${regenerationMark}
    | */
    |buildscript {
    |  dependencies {
    |    classpath 'org.springframework.boot:spring-boot-gradle-plugin:1.+'
    |    classpath 'org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:2.+'
    |    classpath 'org.junit.platform:junit-platform-gradle-plugin:1.+'
    |  }
    |  repositories {
    |    jcenter()
    |  }
    |}
    |
    |apply plugin: 'maven-publish'
    |apply plugin: 'org.springframework.boot'
    |apply plugin: 'org.sonarqube'
    |apply plugin: 'eclipse'
    |apply plugin: 'org.junit.platform.gradle.plugin'
    |
    |bootRun {
    |  addResources = true
    |  jvmArgs = [
    |    "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005",
    |  ]
    |}
    |
    |repositories {
    |  jcenter()
    |}
    |
    |dependencies {
    |  compile     'org.springframework.boot:spring-boot-starter-web'
    |  compile     'org.springframework.boot:spring-boot-starter-data-jpa'
    |  compile     'org.springframework.boot:spring-boot-devtools'
    |  compile     'io.springfox:springfox-swagger2:2.+'
    |  compile     'io.springfox:springfox-swagger-ui:2.+'
    |  compile     'org.apache.commons:commons-lang3:3.+'
    ${csvReportingFunctionUsed ? """\
    |  compile     'org.apache.commons:commons-csv:1.+'
    """ : ''}
    |  compile     'com.miragesql:miragesql:2.+'
    |  compile     'com.h2database:h2'
    ${(dbName == 'postgres') ? """\
    |  compile     'org.postgresql:postgresql:42.+'
    """ : ''}
    ${excelReportingFunctionUsed ? """\
    |  compile     'org.jxls:jxls:2.+'
    |  compile     'org.jxls:jxls-reader:2.+'
    |  compile     'org.jxls:jxls-poi:1.+'
    """ : ''}
    |  testCompile 'org.springframework.boot:spring-boot-starter-test'
    |  testCompile 'junit:junit:4.+'
    |  testRuntime 'org.junit.vintage:junit-vintage-engine:4.+'
    |}
    """.replaceAll(/\n(\s*\n)+/, '\n').stripMargin().trim()
  }

  @Memoized
  String buildOption(String key) {
    options.restServiceGeneratorVariables.getOrDefault(key, '')
  }

  String getDbName() {
    def dbName = buildOption('DB製品名').toLowerCase()
    if (['h2', 'postgres'].contains(dbName)) {
      return dbName
    }
    throw new IllegalArgumentException("Unsupported database: ${dbName}")
  }

  boolean isExcelReportingFunctionUsed() {
    buildOption('Excel帳票機能の使用').toLowerCase() == 'y'
  }

  boolean isCsvReportingFunctionUsed() {
    buildOption('CSV帳票機能の使用').toLowerCase() == 'y'
  }

  @Override
  String getRelPath() {
    'build.gradle'
  }
}
