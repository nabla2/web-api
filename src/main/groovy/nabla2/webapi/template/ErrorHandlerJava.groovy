package nabla2.webapi.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.Project

@Canonical
class ErrorHandlerJava implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """
  |package ${project.namespace.get()};
  |
  |import org.springframework.http.HttpHeaders;
  |import org.springframework.http.ResponseEntity;
  |import org.springframework.web.bind.annotation.ControllerAdvice;
  |import org.springframework.web.bind.annotation.ExceptionHandler;
  |
  |/**
  | * 送信されたリクエスト内容に起因する例外のハンドラ
  | *
  | * @author nabla2.metamodel.generator
  | */
  |@ControllerAdvice
  |public class ${className} {
  |
  |    @ExceptionHandler(InvalidInputException.class)
  |    protected ResponseEntity<Object>
  |    handleUserError(RuntimeException ex) {
  |        InvalidInputException userError = (InvalidInputException) ex;
  |        HttpHeaders headers = new HttpHeaders();
  |        return new ResponseEntity<Object>(
  |            userError.toJson(),
  |            headers,
  |            userError.getStatusCode()
  |        );
  |    }
  |}
  """.trim().stripMargin()
  }

  static String getClassName() {
    "RestErrorHandler"
  }

  @Override
  String getRelPath() {
    "${project.namespace.get().replace('.', '/')}/${className}.java"
  }
}
