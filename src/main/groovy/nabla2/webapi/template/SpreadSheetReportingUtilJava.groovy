package nabla2.webapi.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.webapi.gradle.GenerateRestApiServiceOptionSet
import nabla2.webapi.model.Project

@Canonical
class SpreadSheetReportingUtilJava implements TextFileTemplate {

  Project project

  GenerateRestApiServiceOptionSet options

  @Override
  String getSource() {
    """\
    |package ${project.namespace.get()};
    |
    ${modules.collect{"""\
    |import ${it};
    """.trim()}.join('\n')}
    |/**
    | * 帳票レポート処理用ユーティリティクラス
    | * ${regenerationMark}
    | */
    |public class ${className} {
    |
    |   private ${className}() {}
    |
    |    /**
    |     * 帳票テンプレートからExcel帳票ファイルを生成し、指定されたストリームに出力する。
    |     *
    |     * @param in 帳票テンプレートの入力ストリーム
    |     * @param out 生成されたExcel帳票ファイルを出力するストリーム
    |     * @param context 生成処理の作業情報を格納するコンテキストオブジェクト
    |     */
    |    public static void processPoiTemplate(InputStream in, OutputStream out, Context context) {
    |        Transformer transformer = null;
    |        try {
    |            transformer = PoiTransformer.createTransformer(in, out);
    |
    |        } catch (IOException | InvalidFormatException e) {
    |            throw new IllegalStateException(e); //TODO
    |        }
    |        processTemplate(transformer, new ContextWithCounter(context));
    |    }
    |
    |    /**
    |     * 帳票テンプレートからHandsontableの表示定義およびデータを保持するJSONを生成し、指定されたストリームに出力する。
    |     *
    |     * @param in 帳票テンプレートの入力ストリーム
    |     * @param out Handsontableの表示定義およびデータを保持するJSON
    |     * @param context 生成処理の作業情報を格納するコンテキストオブジェクト
    |     */
    |    public static void processHandsontableTemplate(InputStream in, OutputStream out, Context context) {
    |        Transformer transformer = null;
    |        try {
    |            transformer = HandsontableTransformer.createTransformer(in, out);
    |
    |        } catch (IOException | InvalidFormatException e) {
    |            throw new IllegalStateException(e); //TODO
    |        }
    |        processTemplate(transformer, new ContextWithCounter(context));
    |    }
    |
    |    static private void processTemplate(Transformer transformer, Context context) {
    |        AreaBuilder areaBuilder = new XlsCommentAreaBuilder();
    |        XlsCommentAreaBuilder.addCommandMapping(AllowedValuesCommand.TAG_NAME, AllowedValuesCommand.class);
    |        areaBuilder.setTransformer(transformer);
    |        List<Area> areas = areaBuilder.build();
    |        FormulaProcessor formulaProcessor = new FastFormulaProcessor();
    |        areas.forEach(area -> {
    |            area.applyAt(new CellRef(area.getStartCellRef().getCellName()), context);
    |            area.setFormulaProcessor(formulaProcessor);
    |            area.processFormulas();
    |        });
    |        try {
    |            transformer.write();
    |        }
    |        catch (IOException e) {
    |           // NOP
    |        }
    |    }
    |
    |    /**
    |     * コンテキスト情報の登録処理をインターセプトして付随する情報を自動追加するクラス
    |     */
    |    private static class ContextWithCounter extends Context {
    |        private final Context delegate;
    |        private final Map<String, Integer> counter;
    |
    |        public ContextWithCounter(Context delegate) {
    |            this.delegate = delegate;
    |            this.counter = new HashMap<>();
    |        }
    |
    |        @Override
    |        public Map<String, Object> toMap() {
    |            return delegate.toMap();
    |        }
    |
    |        @Override
    |        public Object getVar(String name) {
    |            return delegate.getVar(name);
    |        }
    |
    |        @Override
    |        public void removeVar(String var) {
    |            delegate.removeVar(var);
    |        }
    |
    |        @Override
    |        public Config getConfig() {
    |            return delegate.getConfig();
    |        }
    |
    |        @Override
    |        public String toString() {
    |            return delegate.toString();
    |        }
    |        @Override
    |        public void putVar(String name, Object value) {
    |            Integer count = counter.get(name);
    |            String indexVarName = name + "_index";
    |            if (count == null) {
    |                counter.put(name, 0);
    |                delegate.putVar(indexVarName, "0");
    |            }
    |            else {
    |                counter.put(name, count + 1);
    |                delegate.putVar(indexVarName, String.valueOf(count + 1));
    |            }
    |            delegate.putVar(name, value);
    |        }
    |    }
    |
    |    /**
    |     * 選択リストによるデータ入力制約を定義するカスタムJXLSタグ
    |     */
    |    public static class AllowedValuesCommand extends AbstractCommand {
    |        public static final String TAG_NAME = "allowedValues";
    |        private String itemsExpression;
    |        private Area area;
    |
    |        @Override
    |        public String getName() {
    |            return TAG_NAME;
    |        }
    |
    |        PoiTransformer getPoiTransformer() {
    |            Transformer transformer = getTransformer();
    |            return (transformer instanceof HandsontableTransformer)
    |                 ? ((HandsontableTransformer) transformer).getPoiTransformer()
    |                 : (PoiTransformer) transformer;
    |        }
    |
    |        @Override
    |        public Size applyAt(CellRef cellRef, Context context) {
    |            Size size = area.applyAt(cellRef, context);
    |            PoiTransformer transformer = getPoiTransformer();
    |            XSSFSheet sheet = (XSSFSheet) transformer.getWorkbook().getSheet(cellRef.getSheetName());
    |            CellRangeAddressList target = new CellRangeAddressList(
    |                cellRef.getRow(), cellRef.getRow(), cellRef.getCol(), cellRef.getCol()
    |            );
    |            XSSFDataValidationHelper helper = new XSSFDataValidationHelper(sheet);
    |            DataValidationConstraint constraint = helper.createExplicitListConstraint(getItems(context));
    |            DataValidation validation = new XSSFDataValidationHelper(sheet).createValidation(constraint, target);
    |            sheet.addValidationData(validation);
    |            return size;
    |        }
    |
    |        @Override
    |        public Command addArea(Area area) {
    |            super.addArea(area);
    |            this.area = area;
    |            return this;
    |        }
    |
    |        public String[] getItems(Context context) {
    |            Object obj = getTransformationConfig().getExpressionEvaluator().evaluate(itemsExpression, context.toMap());
    |            if (obj instanceof Iterable) {
    |                List<String> items = new ArrayList<>();
    |                ((Iterable) obj).forEach(item -> items.add(item == null ? null : item.toString()));
    |                return items.toArray(new String[]{});
    |            }
    |            return (String[]) obj;
    |        }
    |
    |        public void setItems(String expression) {
    |            this.itemsExpression = expression;
    |        }
    |    }
    |
    |    /**
    |     * Handsontableによる画面帳票を実装するTransformer
    |     * ExcelテンプレートからHandsontableのsettings JSONを出力する。
    |     */
    |    private static class HandsontableTransformer implements Transformer {
    |
    |        private final PoiTransformer transformer;
    |        private final List<String> colorPalette;
    |        private final List<String> formats;
    |        private final List<String> constraints;
    |
    |        private static HandsontableTransformer createTransformer(InputStream in, OutputStream out) throws IOException, InvalidFormatException {
    |            return new HandsontableTransformer(in, out);
    |        }
    |
    |        private HandsontableTransformer(InputStream in, OutputStream out) throws IOException, InvalidFormatException {
    |            transformer = PoiTransformer.createTransformer(in, out);
    |            colorPalette = new ArrayList<>();
    |            formats = new ArrayList<>();
    |            constraints = new ArrayList<>();
    |        }
    |
    |        public PoiTransformer getPoiTransformer() {
    |            return this.transformer;
    |        }
    |
    |        @Override
    |        public void write() throws IOException {
    |            AtomicInteger offset = new AtomicInteger(-1);
    |            Workbook workbook = transformer.getWorkbook();
    |            OutputStream out = transformer.getOutputStream();
    |            List<List<Object>> data = new ArrayList<>();
    |            List<List<Object>> bgColor = new ArrayList<>();
    |            List<List<Object>> fgColor = new ArrayList<>();
    |            List<List<Object>> bdColor = new ArrayList<>();
    |            List<List<Object>> locked = new ArrayList<>();
    |            List<List<Integer>> format = new ArrayList<>();
    |            List<Object> constraint = new ArrayList<>();
    |            List<Integer> colOutlineDepth = new ArrayList<>();
    |            Map<String, Object> settings = new HashMap<>();
    |            Map<String, Object> cellProps = new HashMap<>();
    |            cellProps.put("colorPalette", colorPalette);
    |            cellProps.put("formatDefs", formats);
    |            cellProps.put("constraintDefs", constraints);
    |            cellProps.put("bgColor", bgColor);
    |            cellProps.put("fgColor", fgColor);
    |            cellProps.put("bdColor", bdColor);
    |            cellProps.put("locked", locked);
    |            cellProps.put("format", format);
    |            cellProps.put("constraint", constraint);
    |            cellProps.put("colOutlineDepth", colOutlineDepth);
    |            settings.put("data", data);
    |            settings.put("cellProps", cellProps);
    |
    |
    |            workbook.getSheetAt(0).forEach(row -> {
    |                List<Object> rowData = new ArrayList<>();
    |                List<Object> rowBgColor = new ArrayList<>();
    |                List<Object> rowFgColor = new ArrayList<>();
    |                List<Object> rowBdColor = new ArrayList<>();
    |                List<Object> rowLocked = new ArrayList<>();
    |                List<Integer> rowFormat = new ArrayList<>();
    |                offset.compareAndSet(-1, row.getFirstCellNum());
    |                data.add(rowData);
    |                bgColor.add(rowBgColor);
    |                fgColor.add(rowFgColor);
    |                bdColor.add(rowBdColor);
    |                locked.add(rowLocked);
    |                format.add(rowFormat);
    |                row.forEach(cell -> {
    |                    XSSFCellStyle style = (XSSFCellStyle) cell.getCellStyle();
    |                    rowData.add(getValueIn(cell));
    |                    rowBgColor.add(getBgColorOf(style));
    |                    rowFgColor.add(getFgColorOf(style));
    |                    rowBdColor.add(getBorderStyleOf(style));
    |                    rowLocked.add(style.getLocked());
    |                    rowFormat.add(formatOf(style));
    |                });
    |            });
    |            Sheet sheet = workbook.getSheetAt(0);
    |            sheet.getDataValidations().forEach(validation -> {
    |                String[] allowedValues = validation.getValidationConstraint().getExplicitListValues();
    |                if (allowedValues == null) return;
    |                try {
    |                    String constraintDef = new ObjectMapper().writeValueAsString(allowedValues);
    |                    int foundAt = this.constraints.indexOf(constraintDef);
    |                    if (foundAt < 0) {
    |                        constraints.add(constraintDef);
    |                        foundAt = constraints.size() - 1;
    |                    }
    |                    CellRangeAddress range = validation.getRegions().getCellRangeAddress(0);
    |                    constraint.add(new int[]{range.getFirstRow(), range.getFirstColumn() - offset.get(), foundAt});
    |                }
    |                catch (IOException e) {
    |                    throw new RuntimeException(e);
    |                }
    |            });
    |            if (!data.isEmpty()) {
    |                int headerSize = data.stream()
    |                   .max((a, b) -> Integer.compare(a.size(), b.size()))
    |                   .map(List::size).orElse(0);
    |                List<Object> header = data.get(0);
    |                int padWidth = headerSize - header.size();
    |                for (int i = 0; i < padWidth; i++) {
    |                    header.add("");
    |                }
    |                for (int i = 0; i < header.size(); i++) {
    |                    colOutlineDepth.add(i, sheet.getColumnOutlineLevel(i + offset.get()));
    |                }
    |            }
    |            new ObjectMapper().writeValue(out, settings);
    |        }
    |
    |        private Object getValueIn(Cell cell) {
    |            switch (cell.getCellTypeEnum()) {
    |                case BLANK: return null;
    |                case STRING: return cell.getStringCellValue();
    |                case NUMERIC: return cell.getNumericCellValue();
    |                case BOOLEAN: return cell.getBooleanCellValue();
    |                default: return cell.getStringCellValue();
    |            }
    |        }
    |
    |        private Integer formatOf(XSSFCellStyle style) {
    |            String format =  style.getDataFormatString();
    |            Integer foundAt = formats.indexOf(format);
    |            if (foundAt < 0) {
    |                formats.add(format);
    |                return formats.size() - 1;
    |            }
    |            return foundAt;
    |        }
    |
    |        private Integer indexOf(XSSFColor color) {
    |            if (color == null) return null;
    |            String rgb = '#' + Hex.encodeHexString(color.getRGBWithTint());
    |            Integer foundAt = colorPalette.indexOf(rgb);
    |            if (foundAt < 0) {
    |                colorPalette.add(rgb);
    |                return colorPalette.size() - 1;
    |            }
    |            return foundAt;
    |        }
    |
    |        private String getBorderStyleOf(XSSFCellStyle style) {
    |            return indexOf(style.getTopBorderXSSFColor())    + "|"
    |                 + indexOf(style.getRightBorderXSSFColor())  + "|"
    |                 + indexOf(style.getBottomBorderXSSFColor()) + "|"
    |                 + indexOf(style.getLeftBorderXSSFColor());
    |        }
    |
    |        private Integer getBgColorOf(XSSFCellStyle style) {
    |            return indexOf(style.getFillForegroundColorColor());
    |        }
    |
    |        private Integer getFgColorOf(XSSFCellStyle style) {
    |            return indexOf(style.getFont().getXSSFColor());
    |        }
    |
    |        @Override
    |        public void transform(CellRef srcCellRef, CellRef targetCellRef, Context context, boolean updateRowHeight) {
    |            transformer.transform(srcCellRef, targetCellRef, context, updateRowHeight);
    |        }
    |
    |        @Override
    |        public void setFormula(CellRef cellRef, String formulaString) {
    |            transformer.setFormula(cellRef, formulaString);
    |        }
    |
    |        @Override
    |        public Set<CellData> getFormulaCells() {
    |            return transformer.getFormulaCells();
    |        }
    |
    |        @Override
    |        public CellData getCellData(CellRef cellRef) {
    |            return transformer.getCellData(cellRef);
    |        }
    |
    |        @Override
    |        public List<CellRef> getTargetCellRef(CellRef cellRef) {
    |            return transformer.getTargetCellRef(cellRef);
    |        }
    |
    |        @Override
    |        public void resetTargetCellRefs() {
    |            transformer.resetTargetCellRefs();
    |        }
    |
    |        @Override
    |        public void resetArea(AreaRef areaRef) {
    |            transformer.resetArea(areaRef);
    |        }
    |
    |        @Override
    |        public void clearCell(CellRef cellRef) {
    |            transformer.clearCell(cellRef);
    |        }
    |
    |        @Override
    |        public List<CellData> getCommentedCells() {
    |            return transformer.getCommentedCells();
    |        }
    |
    |        @Override
    |        public void addImage(AreaRef areaRef, byte[] imageBytes, ImageType imageType) {
    |            transformer.addImage(areaRef, imageBytes, imageType);
    |        }
    |
    |        @Override
    |        public TransformationConfig getTransformationConfig() {
    |            return transformer.getTransformationConfig();
    |        }
    |
    |        @Override
    |        public void setTransformationConfig(TransformationConfig transformationConfig) {
    |            transformer.setTransformationConfig(transformationConfig);
    |        }
    |
    |        @Override
    |        public boolean deleteSheet(String sheetName) {
    |            return transformer.deleteSheet(sheetName);
    |        }
    |
    |        @Override
    |        public void setHidden(String sheetName, boolean hidden) {
    |            transformer.setHidden(sheetName, hidden);
    |        }
    |
    |        @Override
    |        public void updateRowHeight(String srcSheetName, int srcRowNum, String targetSheetName, int targetRowNum) {
    |            transformer.updateRowHeight(srcSheetName, srcRowNum, targetSheetName, targetRowNum);
    |        }
    |    }
    |}
    """.stripMargin().trim()
  }

  static List<String> getModules() {
    [ 'org.apache.poi.openxml4j.exceptions.InvalidFormatException',
      'org.apache.poi.ss.usermodel.*',
      'org.apache.poi.xssf.usermodel.XSSFCellStyle',
      'org.apache.poi.xssf.usermodel.XSSFColor',
      'org.apache.poi.xssf.usermodel.XSSFSheet',
      'org.apache.poi.ss.util.CellRangeAddress',
      'org.apache.poi.ss.util.CellRangeAddressList',
      'org.apache.poi.xssf.usermodel.XSSFDataValidationHelper',
      'org.jxls.common.*',
      'org.jxls.area.Area',
      'org.jxls.command.Command',
      'org.jxls.builder.AreaBuilder',
      'org.jxls.command.AbstractCommand',
      'org.jxls.builder.xls.XlsCommentAreaBuilder',
      'org.jxls.formula.FastFormulaProcessor',
      'org.jxls.formula.FormulaProcessor',
      'org.jxls.transform.TransformationConfig',
      'org.jxls.transform.Transformer',
      'org.jxls.transform.poi.PoiTransformer',
      'com.fasterxml.jackson.databind.ObjectMapper',
      'org.apache.commons.codec.binary.Hex',
      'java.util.concurrent.atomic.AtomicInteger',
      'java.io.IOException',
      'java.io.InputStream',
      'java.io.OutputStream',
      'java.util.*',
    ].sort().unique()
  }

  static String getClassName() {
    'SpreadSheetReportingUtil'
  }

  @Memoized
  String buildOption(String key) {
    options.restServiceGeneratorVariables.getOrDefault(key, '')
  }

  boolean isReportingFunctionUsed() {
    buildOption('Excel帳票機能の使用').toLowerCase() == 'y'
  }

  @Override
  boolean needsRegenerated(File file) {
    if (!reportingFunctionUsed) {
      return false
    }
    TextFileTemplate.super.needsRegenerated(file)
  }

  @Override
  String getRelPath() {
    "${project.namespace.get().replace('.', '/')}/${className}.java"
  }
}
