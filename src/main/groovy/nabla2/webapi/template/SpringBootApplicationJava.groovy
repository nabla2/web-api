package nabla2.webapi.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.Project

@Canonical
class SpringBootApplicationJava implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """\
  |package ${project.namespace.get()};
  |
  ${modules.collect{"""\
  |import ${it};
  """.trim()}.join('\n')}
  | 
  |/**
  | * ${project.name.get()}のSpringBoot起動クラス
  | *
  | * ${regenerationMark}
  | */
  |@SpringBootApplication
  |@EnableSwagger2
  |public class ${className} extends WebMvcConfigurerAdapter {
  |
  |    @Autowired
  |    private MessageSource messageSource;
  |
  |    public static void main(String... args) {
  |        SpringApplication.run(${className}.class, args);
  |    }
  |
  |    @Bean
  |    public Docket api() {
  |        return new Docket(DocumentationType.SWAGGER_2)
  |              .select()
  |              .apis(RequestHandlerSelectors.any())
  |              .paths(PathSelectors.any())
  |              .build();
  |    }
  |
  |    @Bean
  |    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
  |        ObjectMapper mapper = new ObjectMapper();
  |        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
  |        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(mapper);
  |        return converter;
  |    }
  |
  |    @Bean
  |    public LocalValidatorFactoryBean validator() {
  |      LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
  |      localValidatorFactoryBean.setValidationMessageSource(messageSource);
  |      return localValidatorFactoryBean;
  |    }
  |
  |    @Override
  |    public org.springframework.validation.Validator getValidator() {
  |      return validator();
  |    }
  |}
  """.stripMargin().trim()
  }

  static List<String> getModules() {
    [ 'org.springframework.boot.SpringApplication',
      'org.springframework.boot.autoconfigure.SpringBootApplication',
      'org.springframework.context.annotation.Bean',
      'org.springframework.http.converter.json.MappingJackson2HttpMessageConverter',
      'org.springframework.beans.factory.annotation.Autowired',
      'org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter',
      'org.springframework.validation.beanvalidation.LocalValidatorFactoryBean',
      'org.springframework.context.MessageSource',
      'springfox.documentation.builders.PathSelectors',
      'springfox.documentation.builders.RequestHandlerSelectors',
      'springfox.documentation.spi.DocumentationType',
      'springfox.documentation.spring.web.plugins.Docket',
      'springfox.documentation.swagger2.annotations.EnableSwagger2',
      'com.fasterxml.jackson.databind.ObjectMapper',
      'com.fasterxml.jackson.databind.SerializationFeature',
    ].sort().unique()
  }

  String getClassName() {
    project.className + "Application"
  }

  @Override
  String getRelPath() {
    "${project.namespace.get().replace('.', '/')}/${className}.java"
  }
}
