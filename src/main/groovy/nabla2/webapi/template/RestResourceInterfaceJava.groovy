package nabla2.webapi.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.Project
import nabla2.webapi.model.Resource

@Canonical
class RestResourceInterfaceJava implements TextFileTemplate {

  Resource resource

  String getPayloadsNamespace() {
    resource.namespace.get() +
    '.' +
    resource.identifier
            .lowerCamelized.get()
            .replaceAll(/[A-Z]/) {String head -> '_' + head.toLowerCase()}
  }

  @Memoized
  String getSource() {
  """\
  |package ${resource.namespace};
  |
  ${modules.collect{"""\
  |import ${it};
  """.trim()}.join('\n')}
  |import ${payloadsNamespace}.*;
  |import static ${resource.service.project.namespace.get()}.CommonUtils.*;
  |
  |/**
  | * ${resource.name} リソースインタフェース
  | *
  | * ${regenerationMark}
  | */
  |public interface ${className} {
  ${resource.operations.collect{ operation ->
    String requestClass  = operation.identifier.upperCamelized.get() + 'Request'
    String responseClass = operation.identifier.upperCamelized.get() + 'Response'
    String name = operation.identifier.lowerCamelized.get()
  """\
  |    /**
  |     * ${operation.name}
  |     *
  |     * @param request リクエストオブジェクト
  |     * @return レスポンスオブジェクト
  |     */
  |    ${responseClass} ${name}(${requestClass} request);
  """.trim()}.join('\n')} 
  |
  |}
  """.replaceAll(/\n\s*\n/, '\n').stripMargin().replaceAll(/\n\s*\n/, '\n\n').trim()
  }

  List<String> getModules() {
    ([ usesNumberType() ? 'java.math.BigDecimal' : null,
      'org.hibernate.validator.constraints.NotEmpty',
      'javax.validation.Valid',
      'javax.validation.constraints.Size',
      'javax.validation.constraints.NotNull',
      'javax.validation.constraints.Pattern',
    ] + project.fieldValidators.collect{
      "${project.namespace}.validator.${it.identifier.className.get()}"
    })
    .findAll {it}
    .sort()
    .unique()
  }

  Project getProject() {
    resource.service.project
  }

  boolean usesNumberType() {
    resource.operations*.dataItems.flatten().any {
      it.type.sameAs('number')
    }
  }

  String getClassName() {
    "${resource.identifier.className.get()}Resource"
  }

  @Override
  String getRelPath() {
    "${resource.namespace.get().replace('.', '/')}/${className}.java"
  }
}
