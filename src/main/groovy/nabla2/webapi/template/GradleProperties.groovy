package nabla2.webapi.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.Project

@Canonical
class GradleProperties implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |# ビルド設定
    |# ${regenerationMark}
    |#
    |systemProp.sonar.login=
    |systemProp.sonar.password=
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    'gradle.properties'
  }
}
