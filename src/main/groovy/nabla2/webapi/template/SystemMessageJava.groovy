package nabla2.webapi.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.metamodel.model.LocalizedText
import nabla2.metamodel.model.LocalizedTextBundle
import nabla2.webapi.model.Project

@Canonical
class SystemMessageJava implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |package ${project.namespace.get()};
    |
    ${modules.collect{"""\
    |import ${it};
    """.trim()}.join('\n')}
    |/**
    | * 本システムで使用するメッセージを管理する定数クラス
    | * ${regenerationMark}
    | */
    |public class SystemMessage {
    |
    |    private SystemMessage() {}
    |
    ${dictionary.collect{ key, entry -> """\
    |    /**
    ${entry.collect{lang, text -> """\
    |     * (${lang}): ${text.content.value.map{it.replace('\n', '\n|     |  ')}.orElse('')}
    """.trim()}.join('\n')}
    |     */
    |    public static final String ${key.replace('.', '_').toUpperCase()} = "${key}";
    """.trim()}.join('\n')}
    |
    |    /**
    |     * 指定されたメッセージIDの内容を取得する。
    |     * テキストの言語は現在実行中のリクエストスレッドの処理状況に応じて自動的に決定される。
    |     *
    |     * @param messageId メッセージID
    |     * @param params 埋め込みパラメータ
    |     * @return メッセージの内容
    |     */
    |    public static String textOf(String messageId, Object... params) {
    |        Locale locale = LocaleContextHolder.getLocale();
    |        String message = ResourceBundle
    |              .getBundle("${defaultBundle.resourceBundleId}", locale)
    |              .getString(messageId);
    |
    |        return MessageFormat.format(message, params);
    |    }
    |
    |    /**
    |     * 指定されたメッセージIDの内容(言語は${defaultBundle.locale.get()})を取得する。
    |     *
    |     * @param messageId メッセージID
    |     * @param params 埋め込みパラメータ
    |     * @return メッセージの内容(${defaultBundle.locale.get()})
    |     */
    |    public static String defaultLocaleTextOf(String messageId, Object... params) {
    |         String message = ResourceBundle
    |              .getBundle("${defaultBundle.resourceBundleId}")
    |              .getString(messageId);
    |
    |        return MessageFormat.format(message, params);
    |    }
    |}
    """.stripMargin().trim()
  }

  @Memoized
  LocalizedTextBundle getDefaultBundle() {
    def found = project.systemMessage.find{ it.isDefault.truthy }
    if (!found) throw new IllegalStateException(
      "There is no language which is set as default.\n${project.systemMessage.first()}"
    )
    found
  }

  @Memoized
  Map<String, Map<String, LocalizedText>> getDictionary() {
    Map<String, Map<String, LocalizedText>> dict = new HashMap<>()
    project.systemMessage.forEach { eachLang ->
      eachLang.localizedTextList.forEach { text ->
        def key  = text.key.get()
        def lang = text.locale.get()
        if (dict.containsKey(key)) {
          dict.get(key).put(lang, text)
        }
        else {
          dict.put(key, [(lang):text])
        }
      }
    }
    dict
  }

  static List<String> getModules() {
    [ 'org.springframework.context.i18n.LocaleContextHolder',
      'java.text.MessageFormat',
      'java.util.Locale',
      'java.util.ResourceBundle',
    ].sort().unique()
  }

  static String getClassName() {
    'SystemMessage'
  }

  @Override
  String getRelPath() {
    "${project.namespace.get().replace('.', '/')}/${className}.java"
  }
}
