package nabla2.webapi.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.Operation
import static nabla2.metamodel.datatype.StrictJavaIdentifier.*

@Canonical
class RestOperationLogicJava implements TextFileTemplate {

  Operation operation

  List<String> getDependentModules() {
    [ 'java.util.*',
      'java.util.stream.Collectors',
      'java.math.BigDecimal',
      'javax.persistence.Tuple',
      'org.springframework.beans.factory.annotation.Autowired',
      'org.springframework.stereotype.Component',
    ] + operation.project.tables.collectMany{[
      "${it.namespace}.${it.repositoryClassName}",
      "${it.namespace}.${it.entityClassName}",
    ]}
    .sort().unique()
  }

  @Override
  String getSource() {
    """\
    |package ${operation.namespace};
    |
    ${dependentModules.collect { """\
    |import ${it};
    """.trim()}.join('\n')}
    |
    |/**
    | * ${operation.name}
    | * ${regenerationMark}
    | */
    |@Component
    |public class ${operation.logicClassName} {
    |
    ${operation.project.tables.collect{ table -> """\
    |    /**
    |     * ${table.name}リポジトリ
    |     */
    |    @Autowired
    |    private ${table.repositoryClassName} ${lowerCamelize(table.repositoryClassName)};
    |
    """.trim()}.join('\n')}
    |    /**
    |     * ${operation.name}
    |     */
    |    public ${operation.responsePayloadClassName} execute(${operation.requestPayloadClassName} request) {
    ${operation.mappedRequestItems.collect{it.mapping.targetVariable}.unique().collect{ var -> """\
    |        ${var.snippet.declarationCodeSnippet};
    """.trim()}.join('\n')}
    ${operation.mappedRequestItems.collect{ dataItem -> """\
    |        ${dataItem.mapping.valueAssignmentCodeSnippet('request', 4)};
    """.trim()}.join('\n')}
    |        ${operation.responsePayloadClassName} response = new ${operation.responsePayloadClassName}();
    ${operation.processSteps.collect{ step -> """\
    |        ${step.methodInvocation.replace('\n', '\n|' + ' '*8)};
    """.trim()}.join('\n')}
    |        return response;
    |    }
    ${operation.processSteps.collect{ step -> """\
    |    /**
    |     * ${step.content.literal.get().replace('\n', '\n|     * ')}
    |     */
    |    ${step.methodDeclaration.replace('\n', '\n|'+' '*4)}
    |
    """.trim()}.join('\n')}
    |
    |}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "${operation.namespace.replace('.', '/')}/${operation.logicClassName}.java"
  }
}
