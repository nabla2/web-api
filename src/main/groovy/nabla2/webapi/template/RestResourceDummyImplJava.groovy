package nabla2.webapi.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.Resource

@Canonical
class RestResourceDummyImplJava implements TextFileTemplate {

  Resource resource


  String getPayloadsNamespace() {
    resource.namespace.get() +
    '.' +
    resource.identifier
            .lowerCamelized.get()
            .replaceAll(/[A-Z]/) {String head -> '_' + head.toLowerCase()}
  }
  @Override
  String getSource() {
  """\
  |package ${resource.namespace};
  |
  ${modules.collect{"""\
  |import ${it};
  """.trim()}.join('\n')}
  |import ${payloadsNamespace}.*;
  |
  |/**
  | * ${resource.name} の仮実装
  | *
  | * ${regenerationMark}
  | */
  |@Component
  |public class ${className} implements ${resource.className}Resource {
  |
  ${resource.operations.collect { operation ->
    String name = operation.identifier.lowerCamelized.get()  
    String requestClass  = operation.identifier.className.get() + 'Request'
    String responseClass = operation.identifier.className.get() + 'Response'
    List<DataItem> responseItems = operation.dataItems.findAll {
      it.location.get().contains('レスポンス')
    }
  """\
  |    @Override
  |    public ${responseClass} ${name}(${requestClass} request) {
  ${responseItems.findAll{it.type.sameAs('object')}.reverse().collect { item ->
    String beanName = item.identifier.upperCamelized.get()
    String instanceName = item.identifier.lowerCamelized.get()
  """
  |        ${responseClass}.${beanName} _${instanceName} = new ${responseClass}.${beanName}() {{
  ${item.children.collect{dummySetterOf(it)}.join('\n')}
  |        }};
  """.trim()}.join('\n').replaceAll(/\n\s*\n/, '\n')}
  |        return new ${responseClass}(){{
  ${responseItems.findAll{!it.parent}.collect{dummySetterOf(it)}.join('\n')}
  |        }};
  |    }
  """.trim()
  }.join('\n')}
  |}
  """.stripMargin().trim()
  }

  String getClassName() {
    "${resource.className}ResourceDummyImpl"
  }

  static String dummySetterOf(DataItem item) {
    String propName = item.identifier.upperCamelized.get()
    String pname = item.identifier.lowerCamelized.get()
    String responseClass = item.operation.identifier.className.get() + 'Response'
    switch (item.type) {
    case 'number': return """\
    |            this.set${propName}(new BigDecimal("${item.example.value.orElse('0')}"));
    """.trim()

    case 'string': return """\
    |            this.set${propName}(${item.example.value.map{it.startsWith('"') ? it : ('"' + it.replace('"', '\\"') + '"')}.map{it.replace('\n', '\\n')}.orElse('""')});
    """.trim()

    case 'boolean': return """\
    |            this.set${propName}(Boolean.valueOf(${item.example.value.orElse('false')}));
    """.trim()

    case 'object': return """\
    |            this.set${propName}(_${pname});
    """.trim()

    case 'array': return item.children.first().identifier.with {
      String arrType = it.upperCamelized.get()
      String atype = it.lowerCamelized.get()

    """\
    |            this.set${propName}(new ${responseClass}.${arrType}[]{_${atype}});
    """.trim()}

    }
    throw new IllegalArgumentException(
      "Unknown json type: ${item.type}"
    )
  }

  List<String> getModules() {
    [ 'org.springframework.core.annotation.Order',
      'org.springframework.stereotype.Component',
      usesNumberType() ? 'java.math.BigDecimal' : null,
    ]
    .findAll {it}
    .sort()
    .unique()
  }

  boolean usesNumberType() {
    resource.operations*.dataItems.flatten().any {
      it.type.sameAs('number')
    }
  }

  @Override
  String getRelPath() {
    "${resource.fqn.replace('.', '/')}ResourceDummyImpl.java"
  }
}
