package nabla2.webapi.template

import groovy.json.StringEscapeUtils
import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.Project

@Canonical
class CommonUtilsJava implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
  """
  |package ${project.namespace.get()};
  |
  |import java.math.BigDecimal;
  |import org.apache.commons.lang3.StringUtils;
  |
  |/**
  | * 共通ユーティリティクラス
  | *
  | * ${regenerationMark}
  | */
  |public class ${className} {
  |
  |    /**
  |     * 隠蔽コンストラクタ
  |     */
  |    private ${className}(){}
  |
  |    /**
  |     * 入力文字列を正規化する。
  |     */
  |    public static String normalize(String str) {
  |        if (str == null) return "";
  |        return StringUtils.stripEnd(str, null);
  |    }
  |
  |    /**
  |     * null値であれば{@code 0} に変換して返す。
  |     *
  |     * @param v nullable value
  |     * @return nullsafe value
  |     */
  |    public static Integer nullSafe(Integer v) {
  |        return (v == null) ? 0 : v;
  |    }
  |
  |    /**
  |     * null値であれば{@code 0} に変換して返す。
  |     *
  |     * @param v nullable value
  |     * @return nullsafe value
  |     */
  |    public static BigDecimal nullSafe(BigDecimal v) {
  |        return (v == null) ? BigDecimal.ZERO : v;
  |    }
  |
  |    /**
  |     * null値であれば{@code false} に変換して返す。
  |     *
  |     * @param v nullable value
  |     * @return nullsafe value
  |     */
  |    public static Boolean nullSafe(Boolean v) {
  |        return (v == null) ?  Boolean.FALSE : v;
  |    }
  |
  |    /**
  |     * システム許容文字
  |     */
  |    public static final String ALLOWED_CHARS
  |      = "${StringEscapeUtils.escapeJava(project.charactersAllowedToInput.get())}";
  |}
  """.trim().stripMargin()
  }

  static String getClassName() {
    "CommonUtils"
  }

  @Override
  String getRelPath() {
    "${project.namespace.get().replace('.', '/')}/${className}.java"
  }
}
