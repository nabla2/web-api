package nabla2.webapi.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.Project

@Canonical
class MirageSqlSettingJava implements TextFileTemplate {

  Project project

  @Override
  String getSource() {
    """\
    |package ${project.namespace.get()};
    ${modules.collect{"""\
    |import ${it};
    """.trim()}.join('\n')}
    |/**
    | * ${project.name.get()}のSpringBoot起動クラス
    | *
    | * ${regenerationMark}
    | */
    |@Configuration
    |public class ${className} {
    |    @Bean
    |    ConnectionProvider getConnectionProvider(DataSource dataSource) {
    |        DataSourceTransactionManager tx = new DataSourceTransactionManager();
    |        tx.setDataSource(dataSource);
    |        SpringConnectionProvider bean = new SpringConnectionProvider();
    |        bean.setTransactionManager(tx);
    |        return bean;
    |    }
    |
    |    @Bean
    |    SqlManager getSqlManager(ConnectionProvider connectionProvider) {
    |         SqlManagerImpl bean = new SqlManagerImpl();
    |         bean.setConnectionProvider(connectionProvider);
    |         bean.setDialect(new HyperSQLDialect());
    |         return bean;
    |    }
    |}
    """.stripMargin().trim()
  }

  static List<String> getModules() {
    [ 'com.miragesql.miragesql.SqlManager',
      'com.miragesql.miragesql.SqlManagerImpl',
      'com.miragesql.miragesql.dialect.HyperSQLDialect',
      'com.miragesql.miragesql.integration.spring.SpringConnectionProvider',
      'com.miragesql.miragesql.provider.ConnectionProvider',
      'org.springframework.context.annotation.Bean',
      'org.springframework.context.annotation.Configuration',
      'org.springframework.jdbc.datasource.DataSourceTransactionManager',
      'javax.sql.DataSource',
    ].sort().unique()
  }

  static String getClassName() {
    'MirageSqlSetting'
  }

  @Override
  String getRelPath() {
    "${project.namespace.get().replace('.', '/')}/${className}.java"
  }
}
