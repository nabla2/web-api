package nabla2.webapi.template

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.Operation
import nabla2.webapi.model.Resource

@Canonical
class RestResourceControllerJava implements TextFileTemplate {

  Resource resource

  String getPayloadsNamespace() {
    resource.namespace.get() +
    '.' +
    resource.identifier
            .lowerCamelized.get()
            .replaceAll(/[A-Z]/) {String head -> '_' + head.toLowerCase()}
  }

  @Memoized
  String getSource() {
  """\
  |package ${resource.namespace};
  |
  ${dependingModules.collect{"""\
  |import ${it};
  """.trim()}.join('\n')}
  |import ${payloadsNamespace}.*;
  |import static ${resource.fqn}Resource.*;
  |
  |/**
  | * ${resource.name}リソースコントローラ
  | *
  | * ${regenerationMark}
  | */
  ${resource.service.allowedOrigin.value.map{origins -> """
  |@CrossOrigin(origins = {${origins.split(/,/).collect{"\"${it.trim()}\""}.join(', ')}})
  """.toString()}.orElse('')}
  |@RestController
  |@Api(value="${resource.identifier.get()}/", description="${className} : ${resource.name}リソースコントローラ")
  |public class ${className} {
  |
  |    /**
  |     * ${resource.name}リソースの実装
  |     */
  |    @Autowired
  |    private ${resource.className}Resource provider;
  |
  |    /**
  |     * リソースローダ
  |     */
  |    @Autowired
  |    private ResourceLoader resourceLoader;
  |
  ${resource.operations.collect { operation ->
    boolean hasRequestBody = operation.dataItems.any {
      it.location.sameAs('リクエストボディ') || it.location.sameAs('リクエストパラメータ')
    }
    String requestClass  = operation.identifier.className.get() + 'Request'
    String responseClass = operation.identifier.className.get() + 'Response'
  """\
  |    /**
  |     * ${operation.name}
  |     *
  ${operation.pathParameters.with{ it.empty ? '' : it.collect{ p -> """
  |     * @param ${p.identifier.lowerCamelized.get()} ${p.name}(パスパラメータ)
  """.trim()}.join('\n')}}
  ${hasRequestBody ? """
  |     * @param requestPayload リクエストオブジェクト
  """.trim() : ''}
  |     * @return レスポンスオブジェクト
  |     */
  |    @RequestMapping(
  |        path   = "${getPathFor(operation, operation.pathParameters)}",
  |        method = RequestMethod.${operation.httpMehtod.get().toUpperCase()}
  |    )
  |    @ApiOperation(value="${operation.identifier.lowerCamelized.get()}() : ${operation.name}")
  |    public ${responseClass} ${operation.identifier.lowerCamelized.get()}(
  ${(operation.pathParameters.collect{ """\
  |        @PathVariable("${it.key}") String ${it.key}
  """.trim()} + (!hasRequestBody ? [] : ["""
  |        @Valid ${operation.httpMehtod.get() == 'GET' ? '' : '@RequestBody'} ${requestClass} requestPayload
  """.trim()])).join(',\n')}
  |    ) {
  ${hasRequestBody ? '' : """\
  |        ${requestClass} requestPayload = new ${requestClass}();
  """.trim()}
  ${operation.pathParameters.collect{setterInvokation(it)}.join('\n')}
  |        return provider.${operation.identifier.lowerCamelized.get()}(requestPayload);
  |    }
  ${operation.reports.findAll{it.forDownload}.collect{ report -> """\
  |
  |    /**
  |     * ${report.name}
  |     *
  ${operation.pathParameters.with{ it.empty ? '' : it.collect{ p -> """
  |     * @param ${p.identifier.lowerCamelized.get()} ${p.name}(パスパラメータ)
  """.trim()}.join('\n')}}
  ${hasRequestBody ? """
  |     * @param requestPayload リクエストオブジェクト
  |     * @return レスポンスオブジェクト
  """.trim() : ''}
  |     */
  |    @RequestMapping(
  |        path   = "${getPathFor(operation, operation.pathParameters)}/${report.identifier}.${report.downloadFilesExtension}",
  |        method = RequestMethod.GET
  |    )
  |    @ApiOperation(value="${report.identifier.lowerCamelized.get()}() : ${report.name}")
  |    public void ${report.key}(
  ${(operation.pathParameters.collect{"""\
  |        @PathVariable("${it.key}") String ${it.key}
  """.trim()} + (!hasRequestBody ? [] : ["""
  |        @Valid ${operation.httpMehtod.get() == 'GET' ? '' : '@RequestBody'} ${requestClass} requestPayload
  """.trim()]) + ["""\
  |        HttpServletResponse servletResponse
  """]).join(',\n')}
  |    ) throws IOException {
  ${hasRequestBody ? '' : """\
  |        ${requestClass} requestPayload = new ${requestClass}();
  """.trim()}
  ${operation.pathParameters.collect{setterInvokation(it)}.join('\n')}
  |        ${responseClass} responsePayload = provider.${operation.identifier.lowerCamelized.get()}(requestPayload);
  |        Resource resource = resourceLoader.getResource(
  |            "classpath:${report.layoutFilePath}"
  |        );
  ${report.csvReporting ? """\
  |        String filename = URLEncoder.encode("${report.name}", "UTF-8");
  |        Map<String, Object> context = new HashMap<>();
  |        context.put("request", requestPayload);
  |        context.put("response", responsePayload);
  |        CsvReportingUtil.download(resource.getFile(), context, filename, servletResponse);
  """ : """\
  |        String filename = URLEncoder.encode("${report.name}", "UTF-8");
  |        try (InputStream input = resource.getInputStream()) {
  |            Context context = new Context();
  ${operation.responseBodyTopLevelItems.collect{ item -> """\
  |            context.putVar("${item.key}", responsePayload.${item.getterName}());
  """.trim()}.join('\n')}
  |            context.putVar("request", requestPayload);
  |            context.putVar("response", responsePayload);
  ${(report.downloadFilesExtension != 'json') ? """\
  |            servletResponse.setHeader("Content-disposition", "attachment; filename=" + filename);
  """ : ''}
  |            servletResponse.setContentType("${report.contentType}");
  |            SpreadSheetReportingUtil.process${report.transformerName}Template(input, servletResponse.getOutputStream(), context);
  |            servletResponse.flushBuffer();
  |        }
  """}
  |    }
  """.trim()}.join('\n')}
  ${operation.reports.findAll{it.forUpload}.collect{ report -> """\
  |    /**
  |     * ${report.name}
  ${operation.pathParameters.with{ it.empty ? '' : it.collect{ p -> """
  |     * @param ${p.identifier.lowerCamelized.get()} ${p.name}(パスパラメータ)
  """.trim()}.join('\n')}}
  |     * @param file アップロードファイル
  |     * @return レスポンスオブジェクト
  |     */
  |    @RequestMapping(
  |        path   = "${getPathFor(operation, operation.pathParameters)}/${report.identifier}",
  |        method = RequestMethod.${operation.httpMehtod.get().toUpperCase()}
  |    )
  |    @ApiOperation(value="${report.identifier.lowerCamelized.get()}() : ${report.name}")
  |    public UpdateCardsInDeckResponse ${report.key}(
  ${(operation.pathParameters.collect{ """\
  |        @PathVariable("${it.key}") String ${it.key}
  """.trim()} + ["""\
  |        @RequestParam("file") MultipartFile file
  """]).join(',\n')}
  |    ) throws IOException, SAXException {
  |        ${requestClass} requestPayload = new ${requestClass}();
  ${operation.pathParameters.collect{setterInvokation(it)}.join('\n')}
  |        Resource settings = resourceLoader.getResource(
  |            "classpath:${report.layoutFilePath}"
  |        );
  |        Map<String, Object> values = new HashMap<>();
  |        values.put("request", requestPayload);
  ${report.csvReporting ? """\
  |        CsvReportingUtil.acceptUploadFile(settings.getFile(), values, file);
  """ : """\
  |        XLSReader reader = null;
  |        try (InputStream in = settings.getInputStream()) {
  |            reader = ReaderBuilder.buildFromXML(in);
  |        }
  |        XLSReadStatus status = null;
  |        try (InputStream in = file.getInputStream()) {
  |            status = reader.read(in, values);
  |        }
  |        catch (InvalidFormatException e) {
  |            throw new RuntimeException(e);
  |        }
  |        if (!status.isStatusOK()) {
  |            throw new IllegalArgumentException(status.toString());
  |        }
  """}
  |        return provider.updateCardsInDeck(requestPayload);
  |    }
  """.trim()}.join('\n')}
  """.trim()}.join('\n')}
  |}
  """.replaceAll(/\n\s*\n/, '\n').stripMargin().trim()
  }

  String getPathFor(Operation operation, List<DataItem> params) {
    [ resource.service.basePath,
      resource.basePath,
      operation.path,
    ]
    .join('/')
    .replaceAll(/\/+/, '/')
    .replaceAll(/\{([^{}\/]+)\}/) { m ->
      String key = m[1]
      params.find{ it.name.sameAs(key) }.with {
        "{${ !it ? key : it.identifier.lowerCamelized.get() }}"
      }
    }
  }

  @Memoized
  String getClassName() {
    "${resource.className}Controller"
  }

  @Memoized
  boolean hasExcelReport() {
    resource.operations.any{ it.reports.any{it.excelReporting} }
  }

  @Memoized
  boolean hasCSVReport() {
    resource.operations.any{ it.reports.any{it.csvReporting} }
  }

  List<String> getDependingModules() {
    [ 'javax.validation.Valid',
      'org.springframework.beans.factory.annotation.Autowired',
      'org.springframework.web.bind.annotation.*',
      'org.springframework.core.io.Resource',
      'org.springframework.core.io.ResourceLoader',
      'org.springframework.web.multipart.MultipartFile',
      'io.swagger.annotations.Api',
      'io.swagger.annotations.ApiOperation',
      'org.jxls.common.Context',
      'org.jxls.reader.ReaderBuilder',
      'org.jxls.reader.XLSReadStatus',
      'org.jxls.reader.XLSReader',
      'java.net.URLEncoder',
      'javax.servlet.http.HttpServletResponse',
      'java.io.*',
      'java.util.*',
      'org.apache.poi.openxml4j.exceptions.InvalidFormatException',
      'org.xml.sax.SAXException',
      hasExcelReport() ? "${resource.service.project.namespace}.${SpreadSheetReportingUtilJava.className}" : null,
      hasCSVReport() ? "${resource.service.project.namespace}.${CsvReportingUtilJava.className}" : null,
      usesNumberType() ? 'java.math.BigDecimal' : null,
    ]
    .findAll {it}
    .sort()
    .unique()
  }

  static String setterInvokation(DataItem item) {
    String uName = item.identifier.upperCamelized.get()
    String lName = item.identifier.lowerCamelized.get()

    switch (item.type) {
    case 'string': return """\
    |        requestPayload.set${uName}(${lName});
    """.trim()

    case 'number': return """\
    |        requestPayload.set${uName}(new BigDecimal(${lName}));
    """.trim()

    case 'boolean': return """\
    |        requestPayload.set${uName}(Boolean.valueOf(${lName}));
    """.trim()
    }
    throw new IllegalStateException(
      "Unsupported json type :${item.type}"
    )
  }

  boolean usesNumberType() {
    resource.operations*.dataItems.flatten().any {
      it.type.sameAs('number')
    }
  }

  @Override
  String getRelPath() {
    "${resource.fqn.replace('.', '/')}Controller.java"
  }
}
