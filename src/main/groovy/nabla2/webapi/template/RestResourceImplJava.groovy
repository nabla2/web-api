package nabla2.webapi.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.Resource
import static nabla2.metamodel.datatype.StrictJavaIdentifier.*

@Canonical
class RestResourceImplJava implements TextFileTemplate {

  Resource resource

  List<String> getDependentModules() {
    ([ "${resource.namespace}.${resource.interfaceName}",
      "${resource.payloadsNamespace}.*",
      'org.springframework.beans.factory.annotation.Autowired',
      'org.springframework.stereotype.Component',
      'java.util.List',
      'java.math.BigDecimal',
      'javax.persistence.Tuple',
      'javax.transaction.Transactional',
    ] + resource.operations.collect{
          "${it.namespace}.${it.logicClassName}"
        }
    ).sort().unique()
  }

  @Override
  String getSource() {
    """\
    |package ${resource.namespace};
    |
    ${dependentModules.collect { """\
    |import ${it};
    """.trim()}.join('\n')}
    |
    |/**
    | * ${resource.name}リソースの実装
    | * ${regenerationMark}
    | */
    |@Component
    |public class ${resource.implementerClassName} implements ${resource.interfaceName} {
    |
    ${resource.operations.collect{ operation ->
      String logicClass = operation.logicClassName
      String logicObj = lowerCamelize(logicClass)
    """\
    |    @Autowired
    |    private ${logicClass} ${logicObj};
    |
    |    /**
    |     * ${operation.name}
    |     */
    |    @Override
    |    @Transactional
    |    public ${operation.responsePayloadClassName} ${operation.methodName}(${operation.requestPayloadClassName} request) {
    |        return ${logicObj}.execute(request);
    |    }
    |
    """.trim()}.join('\n')}
    |}
    """.stripMargin().trim()
  }

  @Override
  String getRelPath() {
    "${resource.namespace}.${resource.implementerClassName}".replace('.', '/') + '.java'
  }
}
