package nabla2.webapi.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.persistence.model.DbTable
import nabla2.webapi.model.FieldValidator
import static nabla2.metamodel.datatype.StrictJavaIdentifier.*

@Canonical
class FieldValidatorJava implements TextFileTemplate {

  FieldValidator validator

  @Override
  String getSource() {
  """
  |package ${namespace};
  |
  |import javax.validation.Constraint;
  |import javax.validation.ConstraintValidator;
  |import javax.validation.ConstraintValidatorContext;
  |import javax.validation.Payload;
  |import javax.transaction.Transactional;
  |import java.lang.annotation.Documented;
  |import java.lang.annotation.Retention;
  |import java.lang.annotation.Target;
  |import java.time.LocalDate;
  |import java.time.LocalDateTime;
  |import java.time.format.DateTimeFormatter;
  |import java.time.format.DateTimeParseException;
  |import java.time.format.ResolverStyle;
  |import org.springframework.beans.factory.annotation.Autowired;
  |
  |import static java.lang.annotation.ElementType.*;
  |import static java.lang.annotation.RetentionPolicy.RUNTIME;
  ${tables.empty ? '' : """\
  |import org.springframework.beans.factory.annotation.Autowired;
  ${tables.collect{table -> """\
  |import ${table.namespace}.${table.entityClassName};
  |import ${table.namespace}.${table.repositoryClassName};
  """.trim()}.join('\n')}
  """.trim()}
  |
  |/**
  | * ${validator.name.literal.get().replaceAll(/\n/, '\n| * ')}
  | *
  | * ${regenerationMark}
  | */
  |@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
  |@Retention(RUNTIME)
  |@Constraint(validatedBy = ${className}.Validator.class)
  |@Documented
  |public @interface ${className} {
  |
  ${validator.name.params.collect{param ->
    List<String> segments =  param.split(':')
    String paramTitle = segments[0]
    String paramName = segments[1]
  """
  |    /**
  |     * ${paramTitle}
  |     */
  |    String ${paramName}();
  """.trim()}.join('\n')}
  |
  |    Class<?>[] groups() default {};
  |
  |    Class<? extends Payload>[] payload() default {};
  |
  |    /**
  |     * ${validator.name.literal.get().replaceAll(/\n/, '\n|     * ')}
  |     *
  |     * ${regenerationMark}
  |     */
  |    public static class Validator implements ConstraintValidator<${className}, String> {
  |
  ${tables.collect{table -> """\
  |        /**
  |         * ${table.name}リポジトリ
  |         */
  |        @Autowired
  |        private ${table.repositoryClassName} ${lowerCamelize(table.repositoryClassName)};
  |
  """.trim()}.join('\n')}
  ${validator.name.params.collect{param ->
    List<String> segments =  param.split(':')
    String paramTitle = segments[0]
    String paramName = segments[1]
  """
  |        /**
  |         * ${paramTitle}
  |         */
  |        String ${paramName};
  """.trim()}.join('\n')}
  |        /**
  |         * 初期化処理
  |         */
  |        @Override
  |        public void initialize(${className} annotation) {
  ${validator.name.params.collect{param ->
    List<String> segments =  param.split(':')
    String paramName = segments[1]
  """
  |            this.${paramName} = annotation.${paramName}();
  """.trim()}.join('\n')}
  |        }
  |
  |        /**
  |         * 有効な値であればtrueを返す。
  |         */
  |        @Override
  |        @Transactional
  |        public boolean isValid(String value, ConstraintValidatorContext context) {
  |            ${validator.checkLogic.get().replaceAll(/\n/, '\n|' + (' ' * 12))}
  |        }
  |    }
  |}
  """.trim().stripMargin()
  }

  List<DbTable> getTables() {
    validator.referingTables.value.map{ it.tokenize('\n').collect{ tableName ->
      DbTable found = validator.project.tables.find{it.name.sameAs(tableName)}
      if (!found) throw new IllegalArgumentException(
        "Unknown table name :${tableName}\n${validator}"
      )
      found
    }}.orElse([])
  }

  String getClassName() {
    "${validator.identifier.upperCamelized.get()}"
  }

  String getNamespace() {
    "${validator.project.namespace.get()}.validator"
  }

  @Override
  String getRelPath() {
    "${namespace.replace('.', '/')}/${className}.java"
  }
}
