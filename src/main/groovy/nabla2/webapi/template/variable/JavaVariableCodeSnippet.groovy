package nabla2.webapi.template.variable

/**
 * Java変数に関するコード断片を出力するインタフェース
 */
interface JavaVariableCodeSnippet {

  /**
   * この変数のJava変数型を表す文字列を返す。
   * @return Javaソースコード断片
   */
  String getJavaType()

  /**
   * この変数を宣言するJavaソースコード断片を返す。
   * @return Javaソースコード断片
   */
  String getDeclarationCodeSnippet()

  /**
   * この変数の値を参照するJavaソースコード断片を返す。
   *
   * @param varName この変数の参照名
   * @param itemPath この変数の内部要素を表す文字列
   * @return Javaソースコード断片
   */
  String retrievalCodeSnippet(String varName, String itemPath)

  /**
   * この変数に値を代入するJavaソースコード断片を返す。
   *
   * @param varName この変数の参照名
   * @param itemPath この変数の内部要素を表す文字列
   * @param assignee 設定内容の参照
   * @return Javaソースコード断片
   */
  String assignmentCodeSnippet(String varName, String itemPath, String assignee)
}