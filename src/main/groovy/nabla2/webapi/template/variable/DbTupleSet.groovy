package nabla2.webapi.template.variable

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.persistence.model.DbTable
import nabla2.webapi.model.Variable

import static nabla2.metamodel.datatype.StrictJavaIdentifier.upperCamelize

@Canonical
class DbTupleSet implements JavaVariableCodeSnippet {

  Variable variable

  @Override
  String getJavaType() {
    "List<Tuple>"
  }

  @Override
  String getDeclarationCodeSnippet() {
    String varName = variable.identifier.lowerCamelized.get()
    "${javaType} ${varName} = new ${javaType}()"
  }

  @Override
  String retrievalCodeSnippet(String varName, String itemPath) {
    varName = varName ? varName : variable.identifier.lowerCamelized.get()
    if (!itemPath) return varName
    List<String> tokens = itemPath.tokenize('.').collect{it.trim()}
    Item item = itemOf(tokens.first())
    String retrieval = "${varName}.get(${item.index}, ${item.javaType}.class)"
    item.table
    .map{ table ->
      String itemRetrieval = (tokens.size < 2) ? '' : table.columns.find{it.name.sameAs(tokens.last())}.with { col ->
        if (!col) throw new IllegalArgumentException("Unknown column ${tokens.last()} in table ${tokens.first()}\n${variable}")
        ".get${upperCamelize(col.columnId)}()"
      }
      "${retrieval}${itemRetrieval}".toString()
    }
    .orElse(retrieval)
  }

  @Memoized
  List<Item> getItems() {
    variable.typeDefinition.params.withIndex().collect{ param, i ->
      List<String> tokens = param.tokenize(':').collect{it.trim()}
      new Item(
        name:tokens.first(),
        type:tokens.last(),
        index:i,
        variable: variable,
      )
    }
  }

  @Memoized
  Item itemOf(name) {
    Item found = items.find{ it.name == name }
    if (!found) throw new IllegalArgumentException(
      "Unknown parameter name: ${name}\n${variable}"
    )
    found
  }

  @Override
  String assignmentCodeSnippet(String varName, String itemPath, String assignee) {
    itemPath ? "${varName}.set${upperCamelize(itemPath)}(${assignee})"
             : "${varName }= ${assignee}"
  }

  @Canonical
  static class Item {
    String name
    String type
    int index
    Variable variable

    @Memoized
    Optional<DbTable> getTable() {
      Optional.ofNullable(variable.project.tables.find{ table ->
        table.name.sameAs(type)
      })
    }

    @Memoized
    String getJavaType() {
      table
      .map{ it.entityClassName }
      .orElse(type.contains('数') ? 'Long' : 'String')
    }
  }
}
