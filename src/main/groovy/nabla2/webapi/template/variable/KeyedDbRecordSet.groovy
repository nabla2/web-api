package nabla2.webapi.template.variable

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.persistence.model.DbColumn
import nabla2.persistence.model.DbTable
import nabla2.webapi.model.Variable

import static nabla2.metamodel.datatype.StrictJavaIdentifier.upperCamelize

@Canonical
class KeyedDbRecordSet implements JavaVariableCodeSnippet {

  Variable variable

  @Override
  String getJavaType() {
    "Map<String, List<${table.entityClassName}>>"
  }

  @Override
  String getDeclarationCodeSnippet() {
    "${javaType} ${variable.varName} = new HashMap<>()"
  }

  @Override
  String retrievalCodeSnippet(String varName, String itemPath) {
    if (itemPath == '[key]') {
      return '[key]'
    }
    varName = varName ? varName : variable.identifier.lowerCamelized.get()
    "${varName}${itemPath ? ".get${upperCamelize(toColumnId(itemPath))}()" : ''}"
  }

  @Memoized
  DbTable getTable() {
    variable.typeOptions.first().asTable
  }

  @Memoized
  String toColumnId(String itemPath) {
    DbColumn found = table.columns.find{it.name.sameAs(itemPath)}
    if (!found) throw new IllegalArgumentException(
        "Unkown column name :${itemPath} in table :${table.name}\n${variable}"
    )
    found.columnId
  }

  @Override
  String assignmentCodeSnippet(String varName, String itemPath, String assignee) {
    if (itemPath == '[key]') {
      return '[key]'
    }
    itemPath ? "${varName}.set${upperCamelize(toColumnId(itemPath))}(${assignee})"
             : "${varName} = ${assignee}"
  }
}
