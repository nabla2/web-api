package nabla2.webapi.template.variable

import groovy.transform.Canonical
import nabla2.webapi.model.Variable

@Canonical
class TextData implements JavaVariableCodeSnippet {

  Variable variable

  @Override
  String getJavaType() {
    "String"
  }

  @Override
  String getDeclarationCodeSnippet() {
    String varName = variable.identifier.lowerCamelized.get()
    """\
    ${javaType} ${varName}
    """.trim()
  }

  @Override
  String retrievalCodeSnippet(String varName, String itemPath) {
    varName ? varName : variable.identifier.lowerCamelized.get()
  }

  @Override
  String assignmentCodeSnippet(String varName, String itemPath, String assignee) {
    "${varName} = ${assignee}"
  }
}

