package nabla2.webapi.template

import groovy.transform.Canonical
import nabla2.generator.TextFileTemplate
import nabla2.webapi.model.Project

@Canonical
class InvalidInputExceptionJava implements TextFileTemplate {

  Project project

  static String getClassName() {
    "InvalidInputException"
  }

  @Override
  String getSource() {
  """
  |package ${project.namespace.get()};
  |
  |import com.fasterxml.jackson.core.JsonProcessingException;
  |import com.fasterxml.jackson.databind.ObjectMapper;
  |import org.springframework.http.HttpStatus;
  |
  |import java.io.Serializable;
  |import java.util.*;
  |import java.util.stream.Collectors;
  |
  |/**
  | * REST-APIのリクエスト内容に問題がある場合に送出される実行時例外。
  | *
  | * リクエストを送信したクライアント側に問題があり、問題を修正してリクエストを
  | * 再送信することで正常に処理可能とかんがえられる場合に使用する。
  | *
  | * @author nabla2.metamodel.generator
  | */
  |public class ${className} extends RuntimeException {
  |    /**
  |     * ステータスコード (デフォルト:400)
  |     */
  |    private final HttpStatus statusCode;
  |
  |    private final List<Entry> entries;
  |
  |    static class Entry implements Serializable {
  |        /**
  |         * メッセージID
  |         */
  |        private final String messageId;
  |
  |        /**
  |         * メッセージ中の各プレースホルダに設定される値
  |         */
  |        private final List<Object> messageParams;
  |
  |        /**
  |         * 問題があったとかんがえられる入力項目を表す文字列
  |         */
  |        private final List<String> fieldPaths;
  |
  |         /**
  |          * コンストラクタ
  |          * ステータスコードは400
  |          * @param messageId メッセージID
  |          */
  |        public Entry(String messageId) {
  |            super();
  |            this.messageId = messageId;
  |            messageParams = new ArrayList<>();
  |            fieldPaths = new ArrayList<>();
  |        }
  |
  |        /**
  |         * エラーメッセージを返す。
  |         * @return エラーメッセージ
  |         */
  |        public String getMessage() {
  |            Object[] params = (Object[]) messageParams.toArray(new Object[]{});
  |            return SystemMessage.textOf(messageId, params);
  |        }
  |    }
  |
  |
  |    /**
  |     * デフォルトコンストラクタ
  |     */
  |    public ${className}() {
  |        this(HttpStatus.BAD_REQUEST.value());
  |    }
  |
  |    /**
  |     * コンストラクタ
  |     * @param statusCode ステータスコード
  |     */
  |    public ${className}(int statusCode) {
  |        this.entries = new ArrayList<>();
  |        this.statusCode = HttpStatus.valueOf(statusCode);
  |    }
  |
  |    /**
  |     * エラーがあるかどうかを返す。
  |     * @return エラーがある場合はtrue
  |     */
  |    public boolean hasError() {
  |        return !this.entries.isEmpty();
  |    }
  |
  |    /**
  |     * メッセージを追加する。
  |     * @param messageId メッセージID
  |     * @return このインスタンス自体(for method chain)
  |     */
  |    public ${className} message(String messageId) {
  |        this.entries.add(new Entry(messageId));
  |        return this;
  |    }
  |
  |    /**
  |     * メッセージのプレースホルダに埋め込むパラメータを追加する。
  |     * @param params メッセージパラメータ
  |     * @return このインスタンス自体(for method chain)
  |     */
  |    public ${className} messageParams(Object... params) {
  |        if (entries.isEmpty()) throw new IllegalStateException(
  |            "There is not any message. call #message(id) first."
  |        );
  |        entries.get(entries.size() -1).messageParams.addAll(
  |            Arrays.asList(params)
  |        );
  |        return this;
  |    }
  |
  |    /**
  |     * エラーの要因と考えられるAPIのパラメータのパスを追加する。
  |     * @param paths パス文字列
  |     * @return このインスタンス自体(for method chain)
  |     */
  |    public ${className} fieldPaths(String... paths) {
  |        if (entries.isEmpty()) throw new IllegalStateException(
  |            "There is not any message. call #message(id) first."
  |        );
  |        entries.get(entries.size() -1).fieldPaths.addAll(
  |            Arrays.asList(paths)
  |        );
  |        return this;
  |    }
  |
  |    /**
  |     * エラーメッセージを返す。
  |     * @return エラーメッセージ
  |     */
  |    @Override
  |    public String getMessage() {
  |        return entries.stream()
  |              .map(Entry::getMessage)
  |              .collect(Collectors.joining(", "));
  |    }
  |
  |    /**
  |     * JSON文字列に変換する。
  |     * @return JSON文字列
  |     */
  |    public String toJson() {
  |        Map<String, Object> obj = new HashMap<>();
  |        obj.put("status", this.statusCode.value());
  |        obj.put("errors", entries.stream().map(error -> {
  |            Map<String, Object> entry = new HashMap<>();
  |            entry.put("defaultMessage", error.getMessage());
  |            entry.put("field", error.fieldPaths);
  |            return entry;
  |        }).collect(Collectors.toList()));
  |
  |        try {
  |            return new ObjectMapper().writeValueAsString(obj);
  |        }
  |        catch (JsonProcessingException e) {
  |            throw new RuntimeException(e);
  |        }
  |    }
  |
  |    /**
  |     * HTTPステータスコードを返す。
  |     * @return HTTPステータスコード
  |     */
  |    public HttpStatus getStatusCode() {
  |        return statusCode;
  |    }
  |
  |}
  """.trim().stripMargin()
  }

  @Override
  String getRelPath() {
    "${project.namespace.get().replace('.', '/')}/${className}.java"
  }
}
