package nabla2.webapi.template.step

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.ProcessStep
import nabla2.webapi.model.Variable

@Canonical
class SetValuesToVariable implements JavaMethodCodeSnippet {

  ProcessStep step

  @Override
  String getInvocationCodeSnippet() {
    "this.${step.methodName}(${argumentNames.join(', ')})"
  }

  @Memoized
  List<String> getArgumentNames() {
    step.argumentNames
  }

  @Memoized
  List<String> getArgumentsSignature() {
    step.argumentSignatures
  }

  @Memoized
  Map<String, Variable> getFromVariables() {
    step.content.params[1..-1].inject([:]) { Map<String, Variable> acc, param ->
      List<String> tokens = param.tokenize(':').collect{it.trim()}
      String dataPath = tokens[0]
      String varName = tokens[1]
      acc[dataPath] = findVariable(varName)
      acc
    }
  }

  Variable findVariable(String name) {
    Variable found = step.restOperation.processVariables.find{it.name.sameAs(name)}
    if (!found) throw new IllegalArgumentException(
      "Unkown variable: ${name}\n${step}"
    )
    found
  }

  @Memoized
  Variable getToVariable() {
    String varName = step.content.params[0]
    findVariable(varName)
  }

  @Override
  String getDeclarationCodeSnippet() {
    String varRef = toVariable.varName
    """\
    |protected void ${step.methodName}(${argumentsSignature.join(', ')}) {
    ${fromVariables.collect{itemPath, var -> """\
    |    ${varRef}${toVariable.snippet.assignmentCodeSnippet('', itemPath, var.snippet.retrievalCodeSnippet('', ''))};
    """.trim()}.join('\n')}
    |}
    """.trim().replaceAll(/\n\s*\n/, '\n').stripMargin()
  }
}
