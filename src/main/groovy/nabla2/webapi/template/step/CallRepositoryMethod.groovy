package nabla2.webapi.template.step

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.persistence.model.DbAccessMethod
import nabla2.persistence.model.DbTable
import nabla2.webapi.model.ProcessStep
import nabla2.webapi.model.Variable

import static nabla2.metamodel.datatype.StrictJavaIdentifier.*

@Canonical
class CallRepositoryMethod implements JavaMethodCodeSnippet {

  ProcessStep step

  @Memoized
  DbTable getTable() {
    step.params[0].asTable
  }

  @Memoized
  DbAccessMethod getAccessMethod() {
    String accessName = step.params[1].value
    table.accessMethodWithName(accessName)
  }

  @Override
  String getInvocationCodeSnippet() {
    Variable var = step.returnVariable
    String assignment = var ? "${javaType} ${var.varName} = " : ''
    "${assignment}this.${step.methodName}(${step.argumentNames.join(', ')})"
  }

  @Memoized
  String getJavaType() {
    step.returnVariable.snippet.javaType
  }

  @Override
  String getDeclarationCodeSnippet() {
    String returnType = step.returnVariable ? javaType : 'void'
    """\
    |protected ${returnType} ${step.methodName}(${step.argumentSignatures.join(', ')}) {
    |    ${step.returnVariable ? 'return ' : ''}${lowerCamelize(table.repositoryClassName)}.${lowerCamelize(accessMethod.methodId)}(${step.argumentNames.join(', ')});
    |}
    """.trim().replaceAll(/\n\s*\n/, '\n').stripMargin()
  }


}
