package nabla2.webapi.template.step

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.webapi.model.DataItem
import nabla2.webapi.model.ProcessStep

@Canonical
class SetValuesToResponsePayload implements JavaMethodCodeSnippet {

  ProcessStep step

  @Override
  String getInvocationCodeSnippet() {
    "this.${step.methodName}(${argumentNames.join(', ')})"
  }

  @Memoized
  List<String> getArgumentNames() {
     ['response'] + step.argumentNames
  }

  @Memoized
  List<String> getArgumentsSignature() {
    ["${step.restOperation.responsePayloadClassName} response".toString()] + step.argumentSignatures
  }

  @Override
  String getDeclarationCodeSnippet() {
    List<DataItem> topLevelItems = step.restOperation.responseBodyTopLevelItems
    String returnType = step.returnVariable.with {
      it ? it.snippet.javaType : 'void'
    }
    """\
    |protected ${returnType} ${step.methodName}(${argumentsSignature.join(', ')}) {
    ${topLevelItems.collect{ dataItem -> """\
    |    ${dataItem.mappingJavaCodeSnippet('response').replace('\n', '\n|'+' '*4)}
    """.trim()}.join('\n')}
    |}
    """.trim().replaceAll(/\n\s*\n/, '\n').stripMargin()
  }
}
