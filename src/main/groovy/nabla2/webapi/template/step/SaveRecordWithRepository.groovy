package nabla2.webapi.template.step

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.persistence.model.DbTable
import nabla2.webapi.model.ProcessStep

import static nabla2.metamodel.datatype.StrictJavaIdentifier.lowerCamelize

@Canonical
class SaveRecordWithRepository implements JavaMethodCodeSnippet {

  ProcessStep step

  @Memoized
  DbTable getTable() {
    step.params[0].asTable
  }

  @Override
  String getInvocationCodeSnippet() {
    """\
    |this.${step.methodName}(${step.argumentNames.join(', ')})
    """.trim().replaceAll(/\n\s*\n/, '\n').stripMargin()
  }

  @Override
  String getDeclarationCodeSnippet() {
    """\
    |protected void ${step.methodName}(${step.argumentSignatures.join(', ')}) {
    |    ${lowerCamelize(table.repositoryClassName)}.save(${step.arguments.collect{it.varName}.join(', ')});
    |}
    """.trim().replaceAll(/\n\s*\n/, '\n').stripMargin()
  }


}
