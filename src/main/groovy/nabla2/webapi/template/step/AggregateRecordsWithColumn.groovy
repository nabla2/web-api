package nabla2.webapi.template.step

import groovy.transform.Canonical
import groovy.transform.Memoized
import nabla2.persistence.model.DbColumn
import nabla2.persistence.model.DbTable
import nabla2.webapi.model.ProcessStep
import nabla2.webapi.model.Variable
import static nabla2.metamodel.datatype.StrictJavaIdentifier.*

/**
 * 【変数名】を【キー項目名】をキーとして集約する
 */
@Canonical
class AggregateRecordsWithColumn implements JavaMethodCodeSnippet {

  ProcessStep step

  @Override
  String getInvocationCodeSnippet() {
    Variable var = step.returnVariable
    "${var.snippet.javaType} ${var.varName} = this.${step.methodName}(${step.argumentNames.join(', ')})"
  }

  @Memoized
  List<String> getArgumentNames() {
    step.argumentNames
  }

  @Override
  String getDeclarationCodeSnippet() {
    String returnType = step.returnVariable.with {
      it ? it.snippet.javaType : 'void'
    }
    Variable arg = step.arguments.first()
    DbTable table = arg.typeOptions.first().asTable
    DbColumn column = table.columns.find {
      it.name.sameAs(step.params[1].value)
    }
    String accessor = "${table.entityClassName}::get${upperCamelize(column.columnId)}"
    String argSignature = step.argumentSignatures.first()

    """\
    |protected ${returnType} ${step.methodName}(${argSignature}) {
    |    return ${arg.identifier.lowerCamelized.get()}.stream().collect(Collectors.groupingBy(${accessor}));
    |}
    """.trim().replaceAll(/\n\s*\n/, '\n').stripMargin()
  }
}
