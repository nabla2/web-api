package nabla2.webapi.template.step

/**
 * Javaメソッドに関するコード断片を出力するインタフェース
 */
interface JavaMethodCodeSnippet {
  String getInvocationCodeSnippet()
  String getDeclarationCodeSnippet()
}