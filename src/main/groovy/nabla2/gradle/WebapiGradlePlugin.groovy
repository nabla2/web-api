package nabla2.gradle

import org.gradle.api.Project
import nabla2.gradle.BasePlugin
import nabla2.webapi.gradle.GenerateRestApiServiceWithDummyImpl
import nabla2.webapi.gradle.GenerateRestApiService

/**
 * WebAPI自動生成Gradleプラグイン
 *
 * @author nabla2.metamodel.generator
 */
class WebapiGradlePlugin extends BasePlugin {

  void apply(Project project) {
    register(
      project,
      GenerateRestApiServiceWithDummyImpl,
      GenerateRestApiServiceWithDummyImpl.Settings,
    )
    register(
      project,
      GenerateRestApiService,
      GenerateRestApiService.Settings,
    )
  }
}