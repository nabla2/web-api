package nabla2.webapi.template

import groovy.transform.Memoized
import nabla2.webapi.WebapiEntityLoader
import nabla2.webapi.model.Resource

class RestResourceControllerJavaTest extends GroovyTestCase {

  @Memoized
  static WebapiEntityLoader getLoader() {
    WebapiEntityLoader.from(
      Class.getResourceAsStream('/flashcard.xlsx')
    )
  }

  void testGetSource() {
    Resource card = loader.resource$.test().values().find {
      it.name.sameAs('カード')
    }

    assert card

    new RestResourceControllerJava(card).source.with {
      assert it.contains("""\
      |package nabla2.flashcard.api;
      """.stripMargin().trim())

      assert it.contains("""\
      |import java.math.BigDecimal;
      |import javax.validation.Valid;
      |import org.springframework.beans.factory.annotation.Autowired;
      |import org.springframework.web.bind.annotation.PathVariable;
      |import org.springframework.web.bind.annotation.RequestBody;
      |import org.springframework.web.bind.annotation.RequestMapping;
      |import org.springframework.web.bind.annotation.RequestMethod;
      |import org.springframework.web.bind.annotation.RestController;
      |import static nabla2.flashcard.api.CardResource.*;
      """.stripMargin().trim())

      assert it.contains("""\
      |import static nabla2.flashcard.api.CardResource.*;
      """.stripMargin().trim())

      assert it.contains("""\
      |@RestController
      |public class CardController
      """.stripMargin().trim())

      assert it.contains("""\
      |    @Autowired
      |    private CardResource provider;
      """.stripMargin().trim())

      assert it.contains("""\
      |    @RequestMapping(
      |        path   = "/api/deck/{deckName}/cards/{order}/",
      |        method = RequestMethod.GET
      |    )
      """.stripMargin().trim())

      assert it.contains("""\
      |    public GetCardResponse getCard(
      |        @PathVariable("deckName") String deckName,
      |        @PathVariable("order") String order
      |    ) {
      |        GetCardRequest requestPayload = new GetCardRequest();
      |        requestPayload.setDeckName(deckName);
      |        requestPayload.setOrder(new BigDecimal(order));
      |        return provider.getCard(requestPayload);
      |    }
      """.stripMargin().trim())
    }
  }
}
