package nabla2.webapi.template

import groovy.transform.Memoized
import nabla2.webapi.WebapiEntityLoader
import nabla2.webapi.model.Resource

class RestResourceDummyImplJavaTest extends GroovyTestCase {

  @Memoized
  static WebapiEntityLoader getLoader() {
    WebapiEntityLoader.from(
      Class.getResourceAsStream('/flashcard.xlsx')
    )
  }

  void testGetSourceForResouceWhichHasArrayProps() {
    loader.resource$.test().values().with {
    Resource deck = it.find{ it.name.sameAs('単語帳') }
    assert deck
    new RestResourceDummyImplJava(deck).source.with {
      assert it.contains("""\
      |    @Override
      |    public GetCardsResponse getCards(GetCardsRequest request) {
      """.stripMargin().trim())

      assert it.contains("""\
      |        GetCardsResponse.Card card = new GetCardsResponse.Card() {{
      |            this.setOrder(new BigDecimal("2"));
      |            this.setFront("おもて");
      |            this.setBack("うら");
      |        }};
      """.stripMargin().trim())

      assert it.contains("""\
      |        return new GetCardsResponse(){{
      |            this.setCards(new Card[]{card});
      |        }};
      |    }
      """.stripMargin().trim())}
    }
  }

  void testGetSource() {
    assert loader.constraintViolation$.test().values().empty

    loader.resource$.test().values().with {
      Resource card = it.find{ it.name.sameAs('カード') }
      assert it
      new RestResourceDummyImplJava(card).source.with {
        assert it.contains("""\
        |package nabla2.flashcard.api;
        """.stripMargin().trim())

        assert it.contains("""\
        |import java.math.BigDecimal;
        |import org.springframework.core.annotation.Order;
        |import org.springframework.stereotype.Component;
        """.stripMargin().trim())

        assert it.contains("""\
        |@Component
        |@Order(999)
        |public class CardResourceDummyImpl implements CardResource {
        """.stripMargin().trim())
        assert it.contains("""\
        |    @Override
        |    public GetCardResponse getCard(GetCardRequest request)
        """.stripMargin().trim())
        assert it.contains("""\
        |        GetCardResponse.Card card = new GetCardResponse.Card() {{
        |            this.setOrder(new BigDecimal("2"));
        |            this.setFront("おもて");
        |            this.setBack("うら");
        |        }};
        """.stripMargin().trim())
        assert it.contains("""
        |        return new GetCardResponse(){{
        |            this.setCard(card);
        |        }};
        |    }
        """.stripMargin().trim())
      }
    }
  }
}
