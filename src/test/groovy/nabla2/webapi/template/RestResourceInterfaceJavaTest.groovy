package nabla2.webapi.template

import groovy.transform.Memoized
import nabla2.webapi.WebapiEntityLoader
import nabla2.webapi.model.Resource

class RestResourceInterfaceJavaTest extends GroovyTestCase {

  @Memoized
  static WebapiEntityLoader getLoader() {
    WebapiEntityLoader.from(
      Class.getResourceAsStream('/flashcard.xlsx')
    )
  }

  void testGetSourceOfResourceWhichHasArray() {
    Resource deck = loader.resource$.test().values().find {
      it.name.sameAs('単語帳')
    }
    assert deck
    new RestResourceInterfaceJava(deck).source.with {
      assert it.contains("""\
      |    public static class GetCardsResponse {
      """.stripMargin().trim())

      assert it.contains("""\
      |        private Card[] cards;
      """.stripMargin().trim())

      assert it.contains("""\
      |        public Card[] getCards() {
      |            return this.cards;
      |        }
      """.stripMargin().trim())

      assert it.contains("""\
      |        public void setCards(Card[] cards) {
      |            this.cards= cards;
      |        }
      """.stripMargin().trim())

      assert it.contains("""\
      |        public static class Card {
      """.stripMargin().trim())
    }
  }

  void testGetSource() {
    assert loader.constraintViolation$.test().values().empty

    loader.resource$.test().values().with {
      Resource card = it.find{ it.name.sameAs('カード') }
      assert it
      new RestResourceInterfaceJava(card).source.with {
        assert it.contains("""\
        |public interface CardResource
        """.stripMargin().trim())

        assert it.contains("""\
        |    GetCardResponse getCard(GetCardRequest request);
        """.stripMargin().trim())

        assert it.contains("""\
        |        public static class Card {
        """.stripMargin().trim())

        assert it.contains("""
        |            private BigDecimal order;
        """.stripMargin().trim())

        assert it.contains("""
        |            public BigDecimal getOrder() {
        |                return this.order;
        |            }
        """.stripMargin().trim())

        assert it.contains("""
        |            public Integer getOrderAsInt() {
        |                return this.order.intValue();
        |            }
        """.stripMargin().trim())

        assert it.contains("""
        |            public void setOrder(BigDecimal order) {
        |                this.order = order;
        |            }
        """.stripMargin().trim())

        assert it.contains("""
        |            public void setOrderAsInt(Integer order) {
        |                this.order = BigDecimal.valueOf(order);
        |            }
        """.stripMargin().trim())

        assert it.contains("""
        |            private String front;
        """.stripMargin().trim())

        assert it.contains("""
        |            public String getFront() {
        |                return this.front;
        |            }
        """.stripMargin().trim())

        assert it.contains("""
        |            public void setFront(String front) {
        |                this.front = trim(front);
        |            }
        """.stripMargin().trim())

        assert it.contains("""
        |            private String back;
        """.stripMargin().trim())

        assert it.contains("""
        |            public String getBack() {
        |                return this.back;
        |            }
        """.stripMargin().trim())

        assert it.contains("""
        |            public void setBack(String back) {
        |                this.back = trim(back);
        |            }
        """.stripMargin().trim())

        assert it.contains("""\
        |    public static class GetCardRequest {
        """.stripMargin().trim())

        assert it.contains("""\
        |        private String deckName;
        """.stripMargin().trim())

        assert it.contains("""\
        |        public String getDeckName() {
        |            return this.deckName;
        |        }
        """.stripMargin().trim())

        assert it.contains("""\
        |        public void setDeckName(String deckName) {
        |            this.deckName = trim(deckName);
        |        }
        """.stripMargin().trim())

        assert it.contains("""\
        |        private BigDecimal order;
        """.stripMargin().trim())

        assert it.contains("""\
        |        public BigDecimal getOrder() {
        |            return this.order;
        |        }
        """.stripMargin().trim())

        assert it.contains("""\
        |        public Integer getOrderAsInt() {
        |            return this.order.intValue();
        |        }
        """.stripMargin().trim())

        assert it.contains("""\
        |        public void setOrder(BigDecimal order) {
        |            this.order = order;
        |        }
        """.stripMargin().trim())

        assert it.contains("""\
        |        public void setOrderAsInt(Integer order) {
        |            this.order = BigDecimal.valueOf(order);
        |        }
        """.stripMargin().trim())

        assert it.contains("""\
        |    public static class GetCardResponse {
        """.stripMargin().trim())

        assert it.contains("""\
        |        private Card card;
        """.stripMargin().trim())

        assert it.contains("""\
        |        public Card getCard() {
        |            return this.card;
        |        }
        """.stripMargin().trim())

        assert it.contains("""\
        |        public void setCard(Card card) {
        |            this.card = card;
        |        }
        """.stripMargin().trim())

        assert it.contains("""\
        |        public static class Card {
        """.stripMargin().trim())
      }
    }
  }

}
